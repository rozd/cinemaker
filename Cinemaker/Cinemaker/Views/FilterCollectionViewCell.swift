//
//  FilterCollectionViewCell.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 12/23/15.
//  Copyright © 2015 Max Rozdobudko. All rights reserved.
//

import Foundation
import UIKit
import GPUImage

class LookPreviewCollectionViewCell: UICollectionViewCell
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //--------------------------------------------------------------------------
    


    //--------------------------------------------------------------------------
    //
    //  Variables
    //
    //--------------------------------------------------------------------------

    //-------------------------------------
    //  MARK: Outlets
    //-------------------------------------

    @IBOutlet weak var lookPreviewView: GPUImageView!

    @IBOutlet weak var lookNameLabel: UILabel!
    
    //-------------------------------------
    //  MARK: Ivars
    //-------------------------------------

    private var shadowLayer:CAShapeLayer?;
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Overridden properties
    //
    //--------------------------------------------------------------------------
    
    override var selected: Bool
    {
        didSet
        {
            if self.selected != oldValue
            {
                if self.selected
                {
                    self.lookPreviewView.layer.borderWidth = 4.0;
                    self.lookPreviewView.layer.borderColor = UIColor.whiteColor().CGColor;
                    
                    let radius:CGFloat = 8.0;
                    
                    self.shadowLayer = CAShapeLayer();
                    self.shadowLayer!.frame = CGRectMake(0, 0, self.lookPreviewView.bounds.size.width, self.lookPreviewView.bounds.size.height);
                    self.shadowLayer!.shadowColor = UIColor.blackColor().CGColor;
                    self.shadowLayer!.shadowOffset = CGSizeMake(0.0, 0.0);
                    self.shadowLayer!.shadowOpacity = 0.5;
                    self.shadowLayer!.shadowRadius = radius;
                    
                    self.shadowLayer!.fillRule = kCAFillRuleEvenOdd;
                    
                    let top:CGFloat = radius;
                    let bottom:CGFloat = radius;
                    let left:CGFloat = radius;
                    let right:CGFloat = radius;
                    
                    let largerRect:CGRect = CGRectMake(self.shadowLayer!.bounds.origin.x - left,
                        self.shadowLayer!.bounds.origin.y - top,
                        self.shadowLayer!.bounds.size.width + left + right,
                        self.shadowLayer!.bounds.size.height + top + bottom);
                    
                    let path:CGMutablePath = CGPathCreateMutable();
                    CGPathAddRect(path, nil, largerRect);
                    
                    let cornerRadius:CGFloat = 2.0;
                    
                    let bezier:UIBezierPath = UIBezierPath(roundedRect: self.shadowLayer!.bounds, cornerRadius: cornerRadius);
                    
                    CGPathAddPath(path, nil, bezier.CGPath);
                    CGPathCloseSubpath(path);
                    
                    self.shadowLayer!.path = path;
                    
                    self.lookPreviewView.layer.insertSublayer(self.shadowLayer!, atIndex: 0);
                }
                else
                {
                    self.lookPreviewView.layer.borderWidth = 0.0;
                    
                    self.shadowLayer?.removeFromSuperlayer();
                }
            }
        }
    }
    
    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    //
    //  Overridden methods
    //
    //--------------------------------------------------------------------------

    

    //--------------------------------------------------------------------------
    //
    //  Methods MARK: -
    //
    //--------------------------------------------------------------------------

    //-------------------------------------
    //  MARK: selection
    //-------------------------------------

    func markCellAsSelected()
    {
        self.lookPreviewView.layer.borderWidth = 4.0;
        self.lookPreviewView.layer.borderColor = UIColor.whiteColor().CGColor;

        let radius:CGFloat = 8.0;

        self.shadowLayer = CAShapeLayer();
        self.shadowLayer!.frame = CGRectMake(0, 0, self.lookPreviewView.bounds.size.width, self.lookPreviewView.bounds.size.height);
        self.shadowLayer!.shadowColor = UIColor.blackColor().CGColor;
        self.shadowLayer!.shadowOffset = CGSizeMake(0.0, 0.0);
        self.shadowLayer!.shadowOpacity = 0.5;
        self.shadowLayer!.shadowRadius = radius;

        self.shadowLayer!.fillRule = kCAFillRuleEvenOdd;
        
        let top:CGFloat = radius;
        let bottom:CGFloat = radius;
        let left:CGFloat = radius;
        let right:CGFloat = radius;
        
        let largerRect:CGRect = CGRectMake(self.shadowLayer!.bounds.origin.x - left,
                                           self.shadowLayer!.bounds.origin.y - top,
                                           self.shadowLayer!.bounds.size.width + left + right,
                                           self.shadowLayer!.bounds.size.height + top + bottom);
        
        let path:CGMutablePath = CGPathCreateMutable();
        CGPathAddRect(path, nil, largerRect);
        
        let cornerRadius:CGFloat = 2.0;
        
        let bezier:UIBezierPath = UIBezierPath(roundedRect: self.shadowLayer!.bounds, cornerRadius: cornerRadius);

        CGPathAddPath(path, nil, bezier.CGPath);
        CGPathCloseSubpath(path);

        self.shadowLayer!.path = path;
        
        self.lookPreviewView.layer.insertSublayer(self.shadowLayer!, atIndex: 0);
    }

    func unmarkCellAsSelected()
    {
        self.lookPreviewView.layer.borderWidth = 0.0;

        self.shadowLayer?.removeFromSuperlayer();
    }
}