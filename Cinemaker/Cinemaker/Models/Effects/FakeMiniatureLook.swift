//
//  FakeMiniatureLook.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/16/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

class FakeMiniatureLook : Look
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecyle
    //
    //--------------------------------------------------------------------------
    
    init()
    {
        print("FakeMiniatureLook#init");
    }
    
    deinit
    {
        print("FakeMiniatureLook#init");
    }
    
    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------
    
    var name: String
    {
        return "Miniature";
    }
    
    var filterForPreview:GPUImageOutput = GPUImageFakeMiniatureLookFilter();
    
    var filterForThumbnail:GPUImageOutput = GPUImageFakeMiniatureLookFilter();
    
    var filterForMovie:GPUImageOutput
    {
        return GPUImageFakeMiniatureLookFilter();
    }
}