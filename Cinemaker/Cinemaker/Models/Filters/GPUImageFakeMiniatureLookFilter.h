//
//  GPUImageFakeMiniatureFilter.h
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/16/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

#import <GPUImage/GPUImageFramework.h>

@interface GPUImageFakeMiniatureLookFilter : GPUImageFilterGroup
{
    GPUImageMedianFilter* medianFilter;
    GPUImageSharpenFilter* sharpenFilter;
    GPUImageTiltShiftFilter* tiltShiftFilter;
}

@end
