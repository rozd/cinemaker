//
//  RecordService.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 1/31/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

enum RecordServiceType
{
    case GPURecordServiceType;
    case HVTRecordServiceType;
}

protocol RecordServiceDelegate: class
{
    func recordServiceDidRecordFinish();
    func recordServiceDidRecordFailed(error: NSError);
    func recordServiceDidRecrodTerminate();
}

protocol RecordService
{
    // MARK: Lifecycle

    init(outputURL:NSURL);

    // MARK: Properties
    
    weak var delegate: RecordServiceDelegate? { get set }
    
    // Indicates if Recorder is recording at the moment
    var isRecording:Bool { get }
    
    // indicates if Recorder is in use
    var isProcessing:Bool { get }

    var isFinished:Bool { get }
    
    var currentInput:AnyObject { get }
    
    var currentDuration:CMTime { get }

    var cameraFullscreen:Bool { get set };
    
    var cameraFlashlight:Bool { get set };
    
    var cameraOrientation:UIInterfaceOrientation { get set };

    // MARK:

    func teardown();

    // MARK: Camera methods

    func flipCamera();
    func pauseCamera();
    func resumeCamera();

    // MARK: Record methods

    func pauseRecord();
    func resumeRecord();
    func finishRecord(completion: ((success: Bool, error: NSError?) -> ())?);

    // MARK: Snapshot methods

    func createSnapshot() -> UIImage?
}
