//
//  MovieLooksViewModel.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/2/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

protocol MovieLooksViewModelDelegate : class
{
    func looksViewModelDidSnapshotChange(newSnapshot:GPUImagePicture);
    
    func looksViewModelDidLookChange(newLook: Look);
    func looksViewModelDidLookProgress(progress:Float);
    func looksViewModelDidLookFinish();
    func looksViewModelDidLookFailed(error:NSError);
}

class MovieLooksViewModel : NSObject
{
    //-----------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------------
    
    init(session: MovieSession)
    {
        self.session = session;
    }
    
    deinit
    {
        print("MovieLooksViewModel.deinit");
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //-----------------------------------------------------------------------------

    let session: MovieSession;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //-----------------------------------------------------------------------------
    
    //--------------------------------------
    //  MARK: delegate
    //--------------------------------------
    
    weak var delegate: MovieLooksViewModelDelegate?;
    
    //--------------------------------------
    //  MARK: Preview properties
    //--------------------------------------
    
    var previewTimer:NSTimer?;
    
    var totalPreviewCount:Int
    {
        return self.session.snapshots.count;
    }
    
    var currentPreviewIndex:Int = 0;
    
    var currentSnapshot: GPUImagePicture?
    {
        didSet
        {
            self.delegate?.looksViewModelDidSnapshotChange(self.currentSnapshot!);
        }
    }
    
    var currentThumbnail: GPUImagePicture?;

    //--------------------------------------
    //  MARK: Looks properties
    //--------------------------------------

    var selectedLook: Look!
    {
        return self.session.selectedLook;
    }
    
    //--------------------------------------
    //  MARK: Progress properties
    //--------------------------------------
    
    var progressTimer:NSTimer?;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-----------------------------------------------------------------------------
    
    func reactivate()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieLooksViewModel.applicationDidEnterBackgroundNotificationHandler(_:)), name: UIApplicationDidEnterBackgroundNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieLooksViewModel.applicationWillEnterForegroundNotificationHandler(_:)), name: UIApplicationWillEnterForegroundNotification, object: nil);

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieLooksViewModel.lookChangeNotificationHandler(_:)), name: MovieSessionNotification.LookDidChange.rawValue, object: nil);

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieLooksViewModel.lookDidStartNotificationHandler(_:)), name: MovieSessionNotification.LookDidStart.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieLooksViewModel.lookDidEndNotificationHandler(_:)), name: MovieSessionNotification.LookDidEnd.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieLooksViewModel.lookDidFinishNotificationHandler(_:)), name: MovieSessionNotification.LookDidFinish.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieLooksViewModel.lookDidFailedNotificationHandler(_:)), name: MovieSessionNotification.LookDidFailed.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieLooksViewModel.lookDidTerminateNotificationHandler(_:)), name: MovieSessionNotification.LookDidTerminate.rawValue, object: nil);

        self.resumePreview();
    }
    
    func deactivate()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self);

        self.pausePreview();
    }
    
    //--------------------------------------
    //  Methods: Preview
    //--------------------------------------
    
    func resumePreview()
    {
        if self.previewTimer == nil
        {
            self.previewTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: #selector(MovieLooksViewModel.previewNextFrame), userInfo: nil, repeats: true);
            
            self.previewCurrentImage();
        }
    }
    
    func pausePreview()
    {
        self.previewTimer?.invalidate();
        self.previewTimer = nil;
    }
    
    func previewNextFrame()
    {
        self.currentPreviewIndex += 1;
        
        if (self.currentPreviewIndex >= self.session.snapshots.count)
        {
            self.currentPreviewIndex = 0;
        }
        
        self.previewCurrentImage();
    }

    func previewCurrentImage()
    {
        let snapshotImage: UIImage = self.session.snapshots[self.currentPreviewIndex];
        let thumbnailImage: UIImage = self.session.thumbnails[self.currentPreviewIndex];
        
        dispatch_async(GlobalBackgroundQueue)
        {
            let newSnapshot: GPUImagePicture = GPUImagePicture(image: snapshotImage);
            let newThumbnail: GPUImagePicture = GPUImagePicture(image: thumbnailImage);
        
            dispatch_async(GlobalMainQueue)
            {
                self.currentSnapshot?.removeAllTargets();
                self.currentThumbnail?.removeAllTargets();
                
                self.currentSnapshot = newSnapshot;
                self.currentThumbnail = newThumbnail;
                
                for look in self.session.availableLooks
                {
                    self.currentThumbnail?.addTarget(look.filterForThumbnail as! GPUImageInput);
                }
                
                if UIApplication.sharedApplication().applicationState == .Active
                {
                    self.currentThumbnail?.processImage();
                }
                
                if self.session.selectedLook != nil
                {
                    self.currentSnapshot?.addTarget(self.session.selectedLook!.filterForPreview as! GPUImageInput);
                }
                
                self.currentSnapshot!.processImage();
            }
        }
    }
    
    //--------------------------------------
    //  MARK: Looks
    //--------------------------------------

    func getTotalLooks() -> Int
    {
        return self.session.availableLooks.count;
    }

    func getLookByIndex(index: Int) -> Look
    {
        return self.session.availableLooks[index];
    }
    
    func setupFilterForPreview(look: Look, view:GPUImageView)
    {
        self.currentThumbnail?.removeTarget(look.filterForThumbnail as! GPUImageInput);
        
        look.filterForThumbnail.addTarget(view);

        self.currentThumbnail?.addTarget(look.filterForThumbnail as! GPUImageInput);
    }
    
    func clearFilterFromPreview(look: Look, view:GPUImageView)
    {
        look.filterForThumbnail.removeTarget(view);
    }
    
    func selectLook(look: Look)
    {
        Log("MovieLooksViewModel.selectLook \(look)");

        if self.session.selectedLook != nil
        {
            self.currentSnapshot?.removeTarget(self.session.selectedLook!.filterForPreview as! GPUImageInput);
        }
        
        self.session.selectedLook = look;
        
        if self.session.selectedLook != nil
        {
            self.currentSnapshot?.addTarget(self.session.selectedLook!.filterForPreview as! GPUImageInput);
        }
    }

    func applySelectedLook()
    {
        self.session.applySelectedLook();
    }

    //--------------------------------
    //  MARK: Progress timer
    //--------------------------------
    
    private func scheduleProgressTimer()
    {
        self.progressTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(MovieLooksViewModel.updateProgress), userInfo: nil, repeats: true);
    }
    
    private func teardownProgressTimer()
    {
        self.progressTimer?.invalidate();
        self.progressTimer = nil;
    }
    
    func updateProgress()
    {
        let progress = self.session.looksService.currentProgress;
        
        SVProgressHUD.showProgress(progress);

        self.delegate?.looksViewModelDidLookProgress(progress);
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Notification handlers
    //
    //-----------------------------------------------------------------------------

    // MARK: UIApplication
    
    func applicationDidEnterBackgroundNotificationHandler(notification: NSNotification)
    {
        self.pausePreview();
    }
    
    func applicationWillEnterForegroundNotificationHandler(notification: NSNotification)
    {
        self.resumePreview();
    }

    // MARK: MovieSession

    func lookChangeNotificationHandler(notification:NSNotification)
    {
        self.delegate?.looksViewModelDidLookChange(self.session.selectedLook);
    }

    func lookDidStartNotificationHandler(notification: NSNotification)
    {
        SVProgressHUD.showProgress(0.0);

        self.scheduleProgressTimer();
    }

    func lookDidEndNotificationHandler(notification: NSNotification)
    {
        SVProgressHUD.dismiss();

        self.teardownProgressTimer();
    }

    func lookDidFinishNotificationHandler(notification: NSNotification)
    {
        self.delegate?.looksViewModelDidLookFinish();
    }
    
    func lookDidFailedNotificationHandler(notification: NSNotification)
    {
        self.delegate?.looksViewModelDidLookFailed(NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: notification.userInfo));
    }
    
    func lookDidTerminateNotificationHandler(notification: NSNotification)
    {
        self.delegate?.looksViewModelDidLookFailed(NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: notification.userInfo));
    }
}