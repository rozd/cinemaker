//
//  TweetViewController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 1/27/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import TwitterKit

protocol TweetViewControllerDelegate : class
{
    func tweetViewControllerDidComplete(tweet:TWTRTweet);
    
    func tweetViewControllerDidFailure(error:NSError);
    
    func tweetViewControllerDidCanceled();
}

class TweetViewController: UIViewController, UITextViewDelegate, TweetViewModelDelegate
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //--------------------------------------------------------------------------
    
    //-------------------------------------
    //  Outlets
    //-------------------------------------
    
    @IBOutlet weak var contentBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var screenNameLabel: UILabel!
    
    @IBOutlet weak var accountNameLabel: UILabel!
    
    @IBOutlet weak var captionTextView: UITextView!
    
    @IBOutlet weak var captionPromptLabel: UILabel!
    
    @IBOutlet weak var symbolsLimitLabel: UILabel!
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //--------------------------------------------------------------------------
    
    weak var delegate:TweetViewControllerDelegate?;

    var viewModel:TweetViewModel!;
    
    var tweetText:String?;
    
    var tweetVideoURL:NSURL?;
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //--------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad();

        self.viewModel.delegate = self;

        self.setNeedsStatusBarAppearanceUpdate();
        
        // set caption
        self.captionTextView.delegate = self;
        self.captionTextView.text = self.tweetText;
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated);

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TweetViewController.keyboardWillBeShown(_:)), name: UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TweetViewController.keyboardWillBeHidden(_:)), name: UIKeyboardWillHideNotification, object: nil);

        SVProgressHUD.show();

        self.viewModel.reactivate();

        if self.viewModel.currentSession != nil
        {
            self.captionTextView.becomeFirstResponder();
        }
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated);

        NSNotificationCenter.defaultCenter().removeObserver(self);

        self.viewModel.deactivate();
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        super.touchesBegan(touches, withEvent: event);
        
        // dismisses keyboard
        
        self.view.endEditing(false);
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle
    {
        return .LightContent;
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: - Delegates
    //
    //--------------------------------------------------------------------------

    //-------------------------------------
    //  MARK: UITextViewDelegate
    //-------------------------------------
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool
    {
        let currentCharacterCount = textView.text?.characters.count ?? 0
        
        if (range.length + range.location > currentCharacterCount)
        {
            return false;
        }
        
        let newLength = currentCharacterCount + text.characters.count - range.length
        
        return newLength <= 140;
    }

    func textViewDidChange(textView: UITextView)
    {
        if let enteredText = textView.text
        {
            self.symbolsLimitLabel.text = "\(140 - enteredText.characters.count)";
            
            UIView.animateWithDuration(0.4)
            {
                self.captionPromptLabel.hidden = true;
            }
        }
        else
        {
            self.symbolsLimitLabel.text = "140";
        }
    }
    
    func textViewDidEndEditing(textView: UITextView)
    {
        if textView.text == nil || textView.text.characters.count == 0
        {
            UIView.animateWithDuration(0.5)
            {
                self.captionPromptLabel.hidden = false;
            }
        }
    }

    //-------------------------------------
    //  MARK: TweetViewModelDelegate
    //-------------------------------------

    func tweetViewModelDidLoginSuccess()
    {
        SVProgressHUD.dismiss();
    }

    func tweetViewModelDidLoginCancel()
    {
        SVProgressHUD.dismiss();

        self.delegate?.tweetViewControllerDidCanceled();

        self.dismissViewControllerAnimated(true, completion: nil);
    }

    func tweetViewModelDidUserChange()
    {
        if let user = self.viewModel.currentUser
        {
            dispatch_async(GlobalBackgroundQueue)
            {
                if let imageURL = NSURL(string: user.profileImageURL)
                {
                    if let imageData = NSData(contentsOfURL: imageURL)
                    {
                        if let image = UIImage(data: imageData)
                        {
                            dispatch_async(GlobalMainQueue)
                            {
                                self.profileImageView.image = image;
                            }
                        }
                    }
                }
            }

            self.screenNameLabel.text = user.name;

            self.accountNameLabel.text = user.formattedScreenName;
        }
    }

    func tweetViewModelTweetDidStatusChange(message:String)
    {
        SVProgressHUD.showWithStatus(message);
    }

    func tweetViewModelDidErrorOccurs(error: NSError)
    {
        SVProgressHUD.dismiss();

        self.delegate?.tweetViewControllerDidFailure(error);
    }

    func tweetViewModelTweetDidSucceess(tweet:TWTRTweet)
    {
        SVProgressHUD.showSuccessWithStatus("Done".localized);

        self.delegate?.tweetViewControllerDidComplete(tweet);
    }

    //-------------------------------------
    //  MARK: Handlers
    //-------------------------------------
    
    func keyboardWillBeShown(notification:NSNotification)
    {
        let info = notification.userInfo;
        let kbSize = info![UIKeyboardFrameEndUserInfoKey]!.CGRectValue.size;
        
        self.contentBottomConstraint.constant = kbSize.height;
        
        UIView.animateWithDuration(0.4)
        {
            self.view.layoutIfNeeded();
        }
    }
    
    func keyboardWillBeHidden(notification:NSNotification)
    {
        self.contentBottomConstraint.constant = 0.0;
        
        UIView.animateWithDuration(0.4)
        {
            self.view.layoutIfNeeded();
        }
    }

    //--------------------------------------------------------------------------
    //
    //  MARK: Actions
    //
    //--------------------------------------------------------------------------
    
    @IBAction func closeButtonTapped(sender: AnyObject)
    {
        self.delegate?.tweetViewControllerDidCanceled();
        
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    @IBAction func tweetButtonTapped(sender: AnyObject)
    {
        SVProgressHUD.show();

        self.viewModel.tweet(self.captionTextView.text!, movieURL: self.tweetVideoURL!);
    }
}
