//
//  TimelineTableViewCell.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 3/17/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit
import TwitterKit

@objc protocol TWTRTweetViewExtendedDelegate : NSObjectProtocol
{
    optional func tweetView(tweetView:TWTRTweetView, willFlagTweet tweet:TWTRTweet);
}

class TimelineTableViewCell: TWTRTweetTableViewCell
{
    //-------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-------------------------------------------------------------------------

    @available(iOS 3.0, *)
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.flagButton = UIButton(type: .RoundedRect);

        self.flagButton.setImage(UIImage(named: "Flag"), forState: .Normal);
        self.flagButton.setImage(UIImage(named: "Flag Highlighted"), forState: .Highlighted);
        self.flagButton.adjustsImageWhenHighlighted = false;
        self.flagButton.addTarget(self, action: #selector(TimelineTableViewCell.flagButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside);
        self.flagButton.tintColor = Colors.blueGeyser;
        self.flagButton.hitTestEdgeInsets = UIEdgeInsetsMake(-10, -10, -10, -10);
        self.addSubview(self.flagButton);
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder);
    }

    //-------------------------------------------------------------------------
    //
    //  Variables
    //
    //-------------------------------------------------------------------------

    var flagButton:UIButton!;

    //-------------------------------------------------------------------------
    //
    //  MARK: - Overridden methods
    //
    //-------------------------------------------------------------------------

    override func layoutSubviews()
    {
        if self.tweetView.showActionButtons
        {
            if let shareButton = self.tweetView.valueForKey("_shareButton") as? UIButton
            {
                self.flagButton.hidden = false;
                
                self.flagButton.sizeToFit();

                let x = CGRectGetWidth(self.bounds) - CGRectGetWidth(shareButton.bounds);
                
                let y = CGRectGetHeight(self.bounds) - CGRectGetHeight(shareButton.bounds) - 4;
                
                self.flagButton.frame = CGRectMake(x, y, CGRectGetWidth(shareButton.bounds), CGRectGetHeight(shareButton.bounds));
            }
        }
        else
        {
            self.flagButton.hidden = true;
        }

        super.layoutSubviews();
    }
    
    //-------------------------------------------------------------------------
    //
    //  Methods
    //
    //-------------------------------------------------------------------------

    func flagButtonTapped(sender: AnyObject)
    {
        if let delegate = self.tweetView.delegate as? TWTRTweetViewExtendedDelegate
        {
            if let tweet = self.tweetView.valueForKey("_tweet") as? TWTRTweet
            {
                delegate.tweetView?(self.tweetView, willFlagTweet: tweet);
            }
        }
    }

}
