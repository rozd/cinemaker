//
//  MovieSession.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 1/31/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import TwitterKit

enum MovieSessionNotification : String
{
    case RecordDidStart;
    case RecordDidEnd;
    case RecordDidFinish;
    case RecordDidFailed;
    case RecordDidTerminated;

    case LookDidStart;
    case LookDidEnd;
    case LookDidChange;
    case LookDidFailed;
    case LookDidFinish;
    case LookDidTerminate;
}

class MovieSession: RecordServiceDelegate, LooksServiceDelegate
{
    //-------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-------------------------------------------------------------------------
    
    init(sourcePath: String, outputPath: String)
    {
        if NSFileManager.defaultManager().fileExistsAtPath(sourcePath)
        {
            do
            {
                try NSFileManager.defaultManager().removeItemAtPath(sourcePath);
            }
            catch let error as NSError
            {
                print(error);
            }
            catch
            {
            };
        }
        
        self.sourceMovieURL = NSURL(fileURLWithPath: sourcePath);
        
        if NSFileManager.defaultManager().fileExistsAtPath(outputPath)
        {
            do
            {
                try NSFileManager.defaultManager().removeItemAtPath(outputPath);
            }
            catch let error as NSError
            {
                print(error);
            }
            catch
            {
            };
        }

        self.outputMovieURL = NSURL(fileURLWithPath: outputPath);

        self.availableLooks = [OldMovieLook(), LightLeakLook(), AcidPartyLook(), CGAColorLook(), FakeMiniatureLook()];
        
        self.recordService = GPURecordService(outputURL: self.sourceMovieURL);
        self.recordService.delegate = self;

        self.looksService = LooksService();
        self.looksService.delegate = self;
    }
    
    deinit
    {
        print("MovieSession#deinit");
    }
    
    //-------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //-------------------------------------------------------------------------
    

    
    //-------------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //-------------------------------------------------------------------------
    
    //-------------------------------------
    //  MARK: Config
    //-------------------------------------
    
    let minimumDuration:CMTime = CMTimeMake(48000, 48000); // 1 sec
    let maximumDuration:CMTime = CMTimeMake(10, 1); // 10 sec
    
    let sourceMovieURL: NSURL;
    let outputMovieURL: NSURL;
    
    //-------------------------------------
    //  MARK: Services
    //-------------------------------------

    var recordService: RecordService!;

    var looksService: LooksService!;

    lazy var storageService = StorageService();

    //-------------------------------------
    //  snapshots
    //-------------------------------------
    
    var snapshots: [UIImage] = [];
    
    var thumbnails: [UIImage] = [];

    //-------------------------------------
    //  availableLooks
    //-------------------------------------

    var availableLooks: [Look];

    //-------------------------------------
    //  selectedLook
    //-------------------------------------
    
    var selectedLook: Look!
    {
        didSet
        {
            self.selectedLookAlreadyApplied = false;

            NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidChange.rawValue, object: self);
        }
    }

    private var selectedLookAlreadyApplied:Bool = false;
    
    //-------------------------------------
    //  outputAssetId
    //-------------------------------------
    
    var outputAssetId: String?;

    //-------------------------------------
    //  isSourceMovieCreated
    //-------------------------------------
    
    var isSourceMovieCreated: Bool
    {
        if self.recordService.isProcessing
        {
            return true
        }
        else if NSFileManager.defaultManager().fileExistsAtPath(self.sourceMovieURL.path!)
        {
            let asset = AVAsset(URL: self.sourceMovieURL);
            
            return CMTimeCompare(asset.duration, kCMTimeZero) == 1;
        }
        else
        {
            return false;
        }
    }
    
    //-------------------------------------
    //  isOutputMovieCreated
    //-------------------------------------
    
    var isOutputMovieCreated: Bool
    {
        return NSFileManager.defaultManager().fileExistsAtPath(self.outputMovieURL.path!);
    }
    
    //-------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-------------------------------------------------------------------------
    
    func teardown()
    {
        self.recordService.teardown();
    }
    
    //----------------------------------------
    //  MARK: Settings
    //----------------------------------------
    
    func setDeviceOrientation(deviceOrientation:UIDeviceOrientation)
    {
        var interfaceOrientation:UIInterfaceOrientation!;
        
        switch deviceOrientation
        {
            case .LandscapeLeft :
                interfaceOrientation = .LandscapeRight;
                
            case .LandscapeRight :
                interfaceOrientation = .LandscapeLeft;
                
            default :
                interfaceOrientation = .Portrait;
        }
        
        self.recordService!.cameraOrientation = interfaceOrientation;
    }

    //----------------------------------------
    //  Methods: Record
    //----------------------------------------
    
    func switchRecorder(toRecorderType: RecordServiceType)
    {
        switch toRecorderType
        {
            case .GPURecordServiceType:
                
                if !(self.recordService is GPURecordService)
                {
                    self.recordService = GPURecordService(outputURL: self.sourceMovieURL)
                }
            
            case .HVTRecordServiceType: break
            
                //
        }
    }

    func deactivateRecord()
    {
        Log("MovieSession.deactivateRecord()");

        self.recordService!.pauseCamera();
        self.recordService!.pauseRecord();
        self.recordService!.cameraFlashlight = false;
    }

    func reactivateRecord()
    {
        Log("MovieSession.reactivateRecord()");

        self.recordService!.resumeCamera();
    }

    func pauseRecord()
    {
        Log("MovieSession.pauseRecord()");

        self.recordService!.pauseRecord();
    }
    
    func resumeRecord()
    {
        Log("MovieSession.resumeRecord()");

        if !self.recordService.isRecording
        {
            NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.RecordDidStart.rawValue, object: self);
        }

        if self.recordService.isFinished
        {

        }
        else
        {
            self.recordService!.resumeRecord();
        }
    }
    
    func finishRecord()
    {
        Log("MovieSession.finishRecord()");

        self.recordService!.finishRecord(nil);
    }

    func pauseCamera()
    {
        Log("MovieSession.pauseCamera()");

        self.recordService!.pauseCamera();
    }

    func resumeCamera()
    {
        Log("MovieSession.resumeCamera()");

        self.recordService!.resumeCamera();
    }

    func createSnapshot() -> UIImage?
    {
        return self.recordService!.createSnapshot();
    }

    func toggleCameraPosition()
    {
        Log("MovieSession.toggleCameraPosition()");

        self.recordService!.flipCamera();
    }

    func toggleCameraFlash()
    {
        Log("MovieSession.toggleCameraFlash()");

        self.recordService!.cameraFlashlight = !self.recordService!.cameraFlashlight;
    }

    func toggleCameraFullscreen()
    {
        Log("MovieSession.toggleCameraFullscreen()");

        self.recordService!.cameraFullscreen = !self.recordService!.cameraFullscreen;
    }

    //----------------------------------------
    //  MARK: Looks
    //----------------------------------------
    
    func applySelectedLook()
    {
        Log("MovieSession.applySelectedLook()");

        if self.selectedLookAlreadyApplied
        {
            Log("Selected look is already applied");

            NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidFinish.rawValue, object: self, userInfo: nil);
            return;
        }

        if self.recordService!.isProcessing
        {
            Log("Record is in the process, finish it");

            self.recordService!.finishRecord() { [unowned self] (success: Bool, error: NSError?) -> () in

                if success
                {
                    Log("Record finished, start apply look");

                    NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidStart.rawValue, object: self);

                    self.looksService.applyLook(self.selectedLook, inputMovieURL: self.sourceMovieURL, outputMovieURL: self.outputMovieURL);
                }
                else
                {
                    Log("Record finished with error: \(error!.localizedDescription)");

                    NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidFailed.rawValue, object: self, userInfo: [NSLocalizedDescriptionKey : error!.localizedDescription]);
                }
            }
        }
        else
        {
            if self.isSourceMovieCreated
            {
                Log("Start apply look");

                NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidStart.rawValue, object: self);

                self.looksService.applyLook(self.selectedLook, inputMovieURL: self.sourceMovieURL, outputMovieURL: self.outputMovieURL);
            }
            else //
            {
                Log("Source video file not found");

                NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidFailed.rawValue, object: self, userInfo: [NSLocalizedDescriptionKey : "Can't find movie to apply effect.".localized]);
            }
        }
    }
        
    //----------------------------------------
    //  MARK: Share & Save
    //----------------------------------------
    
    func saveToCameraRoll(completion: (assetId: String?, error: NSError?) -> ())
    {
        self.storageService.saveToCameraRoll(self.outputMovieURL) { [unowned self] (assetId: String?, error: NSError?) -> () in
            
            if error == nil
            {
                self.outputAssetId = assetId;
            }
            
            completion(assetId: assetId, error: error);
        }
    }

    //----------------------------------------
    //  RecorderDelegate
    //----------------------------------------
    
    func recordServiceDidRecordFinish()
    {
        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.RecordDidEnd.rawValue, object: self);
        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.RecordDidFinish.rawValue, object: self);
    }
    
    func recordServiceDidRecordFailed(error:NSError)
    {
        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.RecordDidEnd.rawValue, object: self);
        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.RecordDidFailed.rawValue, object: self, userInfo: error.userInfo);
    }
    
    func recordServiceDidRecrodTerminate()
    {
        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.RecordDidEnd.rawValue, object: self);
        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.RecordDidTerminated.rawValue, object: self, userInfo: nil);
    }
    
    //----------------------------------------
    //  LooksServiceDelegate
    //----------------------------------------
    
    func looksServiceDidFinish()
    {
        self.selectedLookAlreadyApplied = true;

        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidEnd.rawValue, object: self);
        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidFinish.rawValue, object: self, userInfo: nil);
    }

    func looksSerivceDidFailed(error: NSError)
    {
        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidEnd.rawValue, object: self);
        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidFailed.rawValue, object: self, userInfo: error.userInfo);
    }

    func looksServiceDidTerminate()
    {
        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidEnd.rawValue, object: self);
        NSNotificationCenter.defaultCenter().postNotificationName(MovieSessionNotification.LookDidTerminate.rawValue, object: self, userInfo: nil);
    }
}