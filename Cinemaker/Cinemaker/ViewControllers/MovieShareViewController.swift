//
//  MovieShareViewController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/3/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit
import TwitterKit

class ShareViewController: UIViewController, SharingViewModelDelegate, TweetViewControllerDelegate
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //--------------------------------------------------------------------------
    
    deinit
    {
        print("ShareViewController.deinit");
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //--------------------------------------------------------------------------
    
    //------------------------------------
    //  MARK: Outlets
    //------------------------------------
    
    @IBOutlet weak var finalVideoTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var finalVideoView: UIVideoView!
    
    @IBOutlet weak var shareOptionsView: UIView!
    
    @IBOutlet weak var outputVideoWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var outputVideoHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var bannerView: GADBannerView!
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //--------------------------------------------------------------------------
    
    var viewModel: MovieShareViewModel!;
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //--------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        self.viewModel.delegate = self;

        // updates constraints
        
        let viewWidth = CGRectGetWidth(self.view.bounds);
        let viewHeight = CGRectGetHeight(self.view.bounds);
        
        if viewHeight - viewWidth < 156 + 64
        {
            self.finalVideoTopConstraint.constant = viewHeight - (viewWidth + 156 + 64);
        }

        self.outputVideoWidthConstraint.constant = viewWidth;
        self.outputVideoHeightConstraint.constant = viewWidth;
        self.view.layoutSubviews();

        // setup video view
        
        self.finalVideoView.movieURL = self.viewModel.outputMovieURL;
        self.finalVideoView.play();
        
        // setup banner
        
        let request = GADRequest()
        request.testDevices = ["c13b768bc4dd898a676b048988bbee13", kGADSimulatorID];
        
        self.bannerView.adUnitID = "ca-app-pub-4593232388378034/4609040305"; // test: ca-app-pub-3940256099942544/2934735716
        self.bannerView.rootViewController = self;
        self.bannerView.loadRequest(request);
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated);
    }
    
    override func viewDidDisappear(animated: Bool)
    {
        super.viewDidDisappear(animated);
        
        self.finalVideoView.stop();
    }
    
    @available(iOS 5.0, *)
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        super.prepareForSegue(segue, sender: sender);
        
        print("prepare for segue \(segue)");
        
        if let tweetViewController = segue.destinationViewController as? TweetViewController
        {
            let sender:UIView = self.shareOptionsView;
            let rect = self.shareOptionsView.bounds;

            tweetViewController.modalPresentationStyle = UIModalPresentationStyle.Popover;
            tweetViewController.preferredContentSize = CGSizeMake(500, 700);
            tweetViewController.popoverPresentationController?.sourceView = sender;
            tweetViewController.popoverPresentationController?.sourceRect = rect;
            tweetViewController.popoverPresentationController?.backgroundColor = Colors.blueCharade;
            tweetViewController.delegate = self;
            tweetViewController.viewModel = self.viewModel.newTweetViewModel();
            tweetViewController.tweetVideoURL = self.viewModel.outputMovieURL;
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //--------------------------------------------------------------------------
    
    func showErrorAlertWithMessage(message:String)
    {
        let alertController = UIAlertController(title: "Error".localized, message: message, preferredStyle: .Alert);
        
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));
        
        self.presentViewController(alertController, animated: true, completion: nil);
    }
    
    //-------------------------------------
    //  SharingViewModelDelegate
    //-------------------------------------
    
    func sharingViewModelDidSavingToCameraRollComplete()
    {
        // does nothing yet
    }
    
    func sharingViewModelDidSavingToCameraRollFailed(error: NSError)
    {
        self.showErrorAlertWithMessage(error.localizedDescription);
    }
    
    func sharingViewModelDidOpenInstagramError(error: NSError)
    {
        self.showErrorAlertWithMessage(error.localizedDescription);
    }
    
    //-------------------------------------
    //  TweetViewControllerDelegate
    //-------------------------------------
    
    func tweetViewControllerDidComplete(tweet:TWTRTweet)
    {
        self.dismissViewControllerAnimated(true, completion: nil);
        
        self.viewModel.retweetTweetToCinemaker(tweet);
    }
    
    func tweetViewControllerDidFailure(error:NSError)
    {
        self.dismissViewControllerAnimated(true, completion: nil);
        
        self.showErrorAlertWithMessage(error.localizedDescription);
    }
    
    func tweetViewControllerDidCanceled()
    {
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Actions
    //
    //--------------------------------------------------------------------------
    
    @IBAction func onDoneItem(sender: AnyObject)
    {
        self.viewModel.attemptToDone(self, barButtonItem: sender as! UIBarButtonItem) { [unowned self] (interrupt: Bool) -> () in
            
            if !interrupt
            {
                self.dismissViewControllerAnimated(true, completion: nil);
            }
        }
    }
    
    @IBAction func tweetItButtonTapped(sender: AnyObject)
    {
        self.viewModel.shareOnTwitter(self, sender: sender);
    }
    
    @IBAction func shareOnInstagramButtonTapped(sender: AnyObject)
    {
        self.viewModel.shareOnInstagram();
    }
    
    @IBAction func saveToCameraRollButtonTapped(sender: AnyObject)
    {
        self.viewModel.saveToCameraRoll();
    }

}
