//
// Created by Max Rozdobudko on 1/20/16.
// Copyright (c) 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

class UIVideoView : UIView
{
    //--------------------------------------------------------------------------
    //
    //  Constructors & Destructur
    //
    //--------------------------------------------------------------------------

    deinit
    {
        print("UIVideoView.deinit");
    }

    //--------------------------------------------------------------------------
    //
    //  Variables
    //
    //--------------------------------------------------------------------------

    private var videoPlayer:AVPlayer?;

    private var playerLayer:AVPlayerLayer?;

    private var isPlaying:Bool = false;

    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------

    var movieURL:NSURL?
    {
        willSet
        {
            self.videoPlayer?.pause();

            self.playerLayer?.removeFromSuperlayer();
        }
        didSet
        {
            if self.movieURL != nil
            {
                self.videoPlayer = AVPlayer(URL:self.movieURL!);
                self.videoPlayer!.actionAtItemEnd = .None;
                
                self.playerLayer = AVPlayerLayer(player: self.videoPlayer!);
                self.playerLayer!.frame = self.bounds;
                
                self.layer.addSublayer(self.playerLayer!);
            }
        }
    }

    //--------------------------------------------------------------------------
    //
    //  Overridden methods
    //
    //--------------------------------------------------------------------------

    override func layoutSubviews()
    {
        super.layoutSubviews();

        self.playerLayer?.frame = self.bounds;
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        super.touchesBegan(touches, withEvent: event);
        
        if touches.count == 1
        {
            if self.isPlaying
            {
                self.stop();
            }
            else
            {
                self.play();
            }
        }
    }

    //--------------------------------------------------------------------------
    //
    //  Methods
    //
    //--------------------------------------------------------------------------

    //-------------------------------------
    //  MARK: Control methods
    //-------------------------------------

    func play()
    {
        self.isPlaying = true;
        self.videoPlayer?.play();
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UIVideoView.playerItemDidEndReached(_:)), name: AVPlayerItemDidPlayToEndTimeNotification, object: nil);
    }

    func stop()
    {
        self.isPlaying = false;
        self.videoPlayer?.pause();
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: nil);
    }
    
    //-------------------------------------
    //  MARK: Notification
    //-------------------------------------

    func playerItemDidEndReached(notification:NSNotification)
    {
        self.videoPlayer?.seekToTime(kCMTimeZero);
    }
}
