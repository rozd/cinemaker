//
//  Colors.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 1/27/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

class Colors
{
    class var blueCharade:UIColor
    {
        return UIColor(red: 30/255, green: 30/255, blue: 40/255, alpha: 1.0);
    }
    
    class var blueWaikawaGraya:UIColor
    {
        return UIColor(red: 100/255, green: 115/255, blue: 160/255, alpha: 1.0);
    }
    
    class var blueBayoux:UIColor
    {
        return UIColor(red: 80/255, green: 90/255, blue: 130/255, alpha: 1.0);
    }
    
    class var blueMarguerite:UIColor
    {
        return UIColor(red: 100/255, green: 100/255, blue: 200/255, alpha: 1.0);
    }
    
    class var blueIndigo:UIColor
    {
        return UIColor(red: 93/255, green: 118/255, blue: 193/255, alpha: 1.0);
    }
    
    class var blueGeyser:UIColor
    {
        return UIColor(red: 204/255, green: 214/255, blue: 221/255, alpha: 1.0);
    }
    
    class var greenScreaming:UIColor
    {
        return UIColor(red: 80/255, green: 1.0, blue: 123/255, alpha: 1.0);
    }
    
    class var greenGrayNurse:UIColor
    {
        return UIColor(red: 233/255, green: 240/255, blue: 233/255, alpha: 1.0);
    }
    
    class var redChestnutRose:UIColor
    {
        return UIColor(red: 210/255, green: 85/255, blue: 85/255, alpha: 1.0);
    }
    
    class var redSoftPeach:UIColor
    {
        return UIColor(red: 245/255, green: 238/255, blue: 238/255, alpha: 1.0);
    }
    
    class var purpleBonJour:UIColor
    {
        return UIColor(red: 223/255, green: 220/255, blue: 223/255, alpha: 1.0);
    }
    
    class var grayScorpion:UIColor
    {
        return UIColor(white: 58/255, alpha: 1.0);
    }
    
    class var grayDove:UIColor
    {
        return UIColor(white: 100/255, alpha: 1.0);
    }
    
    class var grayTundora:UIColor
    {
        return UIColor(white: 70/255, alpha: 1.0);
    }
    
    //
    
    class var lightBackgroundColor:UIColor
    {
        return redSoftPeach;
    }
    
    class var lightPrimaryTextColor:UIColor
    {
        return blueCharade;
    }
    
    class var backgroundColor:UIColor
    {
        return blueCharade;
    }
    
    class var promptColor:UIColor
    {
        return blueWaikawaGraya;
    }
    
    class var inactiveColor:UIColor
    {
        return blueBayoux;
    }
    
    class var contrastColor:UIColor
    {
        return greenScreaming;
    }
    
    class var primaryTextColor:UIColor
    {
        return purpleBonJour;
    }
}