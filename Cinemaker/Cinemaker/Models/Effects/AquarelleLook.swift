//
//  AquarelleLook.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/13/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

class AquarelleLook : Look
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecyle
    //
    //--------------------------------------------------------------------------
    
    init()
    {
        print("AquarelleLook#init");
    }
    
    deinit
    {
        print("AquarelleLook#init");
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //--------------------------------------------------------------------------
    
    var name: String
    {
        return "Aquarelle";
    }
    
    var filterForPreview:GPUImageOutput = GPUImageAquarelleLookFilter();
    
    var filterForThumbnail:GPUImageOutput = GPUImageAquarelleLookFilter();
    
    var filterForMovie:GPUImageOutput
    {
        return GPUImageAquarelleLookFilter();
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //--------------------------------------------------------------------------
    
    
}