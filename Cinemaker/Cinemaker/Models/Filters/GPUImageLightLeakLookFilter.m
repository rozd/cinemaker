//
//  GPUImageLightLeakLookFilter.m
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/13/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

#import "GPUImageLightLeakLookFilter.h"

@implementation GPUImageLightLeakLookFilter
{
    GPUImageLightLeakLookFilterMode _filterMode;
}

@synthesize overlayMovie = _overlayMovie;
@synthesize overlayPicture = _overlayPicture;

#pragma mark Lifecycle

- (id)init:(GPUImageLightLeakLookFilterMode)mode
{
    if (!(self = [super init]))
    {
        return nil;
    }
    
    _filterMode = mode;
    
    overlayFilter = [[GPUImageScreenBlendFilter alloc] init];
    [self addFilter:overlayFilter];
    
    if (_filterMode == GPUImageLightLeakLookFilterModeMovie)
    {
        NSURL *overlayMovieURL = [[NSBundle mainBundle] URLForResource:@"light_leak_organic" withExtension:@"mov"];
        
        _overlayMovie = [[THImageMovie alloc] initWithURL:overlayMovieURL];
        _overlayMovie.shouldRepeat = YES;
        
        [_overlayMovie addTarget:overlayFilter atTextureLocation:1];
    }
    else
    {
        NSURL *overlayImageURL = [[NSBundle mainBundle] URLForResource:@"light_leak_organic" withExtension:@"png"];
        
        _overlayPicture = [[GPUImagePicture alloc] initWithURL:overlayImageURL];
        
        [_overlayPicture addTarget:overlayFilter atTextureLocation:1];
    }
    
    self.initialFilters = @[overlayFilter];
    self.terminalFilter = overlayFilter;
    
    return self;
}

-(void)dealloc
{
    NSLog(@"GPUImageLightLeakLookFilter#dealloc");
}

#pragma mark -
#pragma mark Overridden methods

- (void)newFrameReadyAtTime:(CMTime)frameTime atIndex:(NSInteger)textureIndex;
{
    [super newFrameReadyAtTime:frameTime atIndex:textureIndex];
    
    if (_filterMode == GPUImageLightLeakLookFilterModeImage && textureIndex == 0)
    {
        [_overlayPicture processImage];
    }
}

#pragma mark -
#pragma mark GPUImageLookFilter

- (NSArray<THImageMovie*>*) overlays
{
    return @[_overlayMovie];
}

@end
