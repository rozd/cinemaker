//
//  AccountInfoViewController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/6/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit
import TwitterKit

class AccountInfoViewController: UIViewController, AccountInfoViewModelDelegate
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //--------------------------------------------------------------------------
    
    deinit
    {
        print("AccountInfoViewController#deinit");
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Outlets
    //
    //--------------------------------------------------------------------------
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var screenNameLabel: UILabel!
    
    @IBOutlet weak var accountNameLabel: UILabel!
    
    @IBOutlet weak var loginButtom: TWTRLogInButton!
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //--------------------------------------------------------------------------
    
    var viewModel:AccountInfoViewModel!;

    //--------------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //--------------------------------------------------------------------------

    //------------------------------------
    //  UIViewController
    //------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        self.viewModel.delegate = self;
        
        self.loginButtom.logInCompletion = self.viewModel.loginButtonHandler;
        self.loginButtom.hidden = true;
        
//        self.profileImageView.layer.masksToBounds = true;
        
        let borderLayer = CALayer();
        borderLayer.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(self.profileImageView.bounds), CGRectGetHeight(self.profileImageView.bounds));
        borderLayer.cornerRadius = 64.0;
        borderLayer.borderWidth = 6.0;
        borderLayer.borderColor = UIColor.whiteColor().CGColor;
        
        self.profileImageView.layer.addSublayer(borderLayer);
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated);
        
        self.viewModel.reactivate();
        
        self.screenNameLabel.text = self.viewModel.userScreenName;
    }

    override func viewDidDisappear(animated: Bool)
    {
        super.viewDidDisappear(animated);

        self.viewModel.deactivate();
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //--------------------------------------------------------------------------
    
    //-------------------------------------
    //  MARK: AccountInfoViewModelDelegate
    //-------------------------------------
    
    func accountInfoDidUserChange()
    {
        self.screenNameLabel.text = self.viewModel.userScreenName;
        
        if let userPictureURL = self.viewModel.userPictureURL
        {
            dispatch_async(GlobalBackgroundQueue)
            {
                if let imageURL = NSURL(string: userPictureURL)
                {
                    if let imageData = NSData(contentsOfURL: imageURL)
                    {
                        if let image = UIImage(data: imageData)
                        {
                            dispatch_async(GlobalMainQueue)
                            {
                                self.profileImageView.image = image;
                            }
                        }
                    }
                }
            }
            
            self.loginButtom.hidden = true;
        }
        else
        {
            self.profileImageView.image = UIImage(named: "Anonymous".localized);
            
            self.loginButtom.hidden = false;
        }
    }
    
    func accountInfoDidErrorOccurs(error: NSError)
    {
        let alertController = UIAlertController(title: "Error".localized, message: error.localizedDescription, preferredStyle: .Alert);
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));
        
        self.presentViewController(alertController, animated: true, completion: nil);
    }
    
    //-------------------------------------
    //  MARK: Unwind
    //-------------------------------------

    @IBAction func prepareToUnwind(segue: UIStoryboardSegue)
    {

    }
}
