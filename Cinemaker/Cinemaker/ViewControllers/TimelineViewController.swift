//
//  TimelineViewController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 1/27/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import TwitterKit

class TimelineViewController: TWTRTimelineViewController, TWTRTweetViewDelegate, TWTRTweetViewExtendedDelegate, TimelineViewModelDelegate
{
    //-----------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //-----------------------------------------------------------------------
    
    var viewModel: TimelineViewModel!;
    
    //-----------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //-----------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad();

        self.viewModel.delegate = self;

        self.tableView.registerClass(TimelineTableViewCell.self, forCellReuseIdentifier: "TweetCell");

        self.showTweetActions = true;

//        Uncomment if TabBar is translucent
//        if let tabBarHeight = self.tabBarController?.tabBar.bounds.height
//        {
//            let adjustForTabBarInsets = UIEdgeInsetsMake(0, 0, tabBarHeight, 0);
//            
//            self.tableView.contentInset = adjustForTabBarInsets;
//            self.tableView.scrollIndicatorInsets = adjustForTabBarInsets;
//        }
        
        let client = TWTRAPIClient(userID: Twitter.sharedInstance().sessionStore.session()?.userID);
        
        self.dataSource = TWTRUserTimelineDataSource(screenName: self.viewModel.twitterUser, APIClient: client);
//        self.dataSource = TWTRCollectionTimelineDataSource(collectionID: self.viewModel.twitterCollectionId, APIClient: client);
//        self.dataSource = TWTRSearchTimelineDataSource(searchQuery: "@cinemakerapp", APIClient: client);
    }

    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated);

        self.viewModel.reactivate();
        
        self.dataSource.APIClient = TWTRAPIClient(userID: Twitter.sharedInstance().sessionStore.session()?.userID);
    }

    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated);

        self.viewModel.deactivate();
    }

    @available(iOS 2.0, *)
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = super.tableView(tableView, cellForRowAtIndexPath: indexPath);

        return cell;
    }

    @available(iOS 2.0, *)
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        super.tableView(tableView, willDisplayCell: cell, forRowAtIndexPath: indexPath);

        print(cell);

        cell.separatorInset = UIEdgeInsetsZero;
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;

        if let tweetView = cell.contentView as? TWTRTweetView
        {
            tweetView.delegate = self;
        }
    }
    
    //-----------------------------------------------------------------------
    //
    //  MARK: - Delegates
    //
    //-----------------------------------------------------------------------
    
    // MARK: TWTRTweetViewExtendedDelegate
    
    func tweetView(tweetView:TWTRTweetView, willFlagTweet tweet:TWTRTweet)
    {
        self.viewModel.flagTweet(tweet, viewController: self, sourceView: tweetView);
    }

    // MARK: TimelineViewModelDelegate

    func timelineViewModelDidBlockUserSuccess(screenName: String)
    {
        let infoController = UIAlertController(title: "@{user} blocked".localized.substitute(["user" : screenName]), message: "@{name} was blocked, if you believe that their Tweets contains an inappropriate content, please report them for us.".localized.substitute(["name" : screenName]), preferredStyle: .Alert);
        infoController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));
        
        self.refresh();
        
        self.presentViewController(infoController, animated: true, completion: nil);
    }
    
    func timelineViewModelDidReportIssueSuccess()
    {
        let infoController = UIAlertController(title: nil, message: "Thank you! We received your report.".localized, preferredStyle: .Alert);
        infoController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));
        
        self.presentViewController(infoController, animated: true, completion: nil);
    }

    func timelineViewModelDidReportTweetSuccess()
    {
        let infoController = UIAlertController(title: nil, message: "Thank you! Your report was received, we removed the Tweet from Timeline and will check if it conforms to our rules, after that it could appear again.".localized, preferredStyle: .Alert);
        infoController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));
        
        self.refresh();
        
        self.presentViewController(infoController, animated: true, completion: nil);
    }

    func timelineViewModelDidErrorOccurs(error: NSError)
    {
        let errorController = UIAlertController(title: "Error".localized, message: error.localizedDescription, preferredStyle: .Alert);
        errorController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));
        
        self.presentViewController(errorController, animated: true, completion: nil);
    }

}
