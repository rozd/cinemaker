//
// Created by Max Rozdobudko on 12/29/15.
// Copyright (c) 2015 Max Rozdobudko. All rights reserved.
//

#import <GPUImage/GPUImageFramework.h>

#import "GPUImageOldMovieLookFilter.h"

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
NSString *const kGPUImageOldMovieLookFragmentShaderString = SHADER_STRING
(
    precision highp float;

    uniform float time;
    varying highp vec2 textureCoordinate;
    varying highp vec2 textureCoordinate2;

    uniform sampler2D inputImageTexture;
    uniform sampler2D inputImageTexture2;

//    const highp float exposure = 2;

    const highp vec3 W = vec3(0.2125, 0.7154, 0.0721);

    float snoise(vec2 co){
        return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
    }

    void main()
    {
        float n = snoise(vec2(textureCoordinate.x*time,textureCoordinate.y*time));

        highp vec4 base = texture2D(inputImageTexture, textureCoordinate);

        float luminance = dot(base.rgb * pow(2.0, 2.0), W);

        base = vec4(vec3(luminance), base.a);
//        base = vec4(vec3(luminance) * pow(2.0, 2.0), base.a);

        highp vec4 overlayer = texture2D(inputImageTexture2, textureCoordinate2);

        highp vec4 result = overlayer * base + overlayer * (1.0 - base.a) + base * (1.0 - overlayer.a);

        gl_FragColor = result * vec4(0.5 + 0.5 * vec3(n, n, n), 1.0);
    }
);
//NSString *const kGPUImageOldMovieLookFragmentShaderString = SHADER_STRING
//(
//    varying highp vec2 textureCoordinate;
//    varying highp vec2 textureCoordinate2;
//
//    uniform sampler2D inputImageTexture;
//    uniform sampler2D inputImageTexture2;
//
//    void main()
//    {
//        lowp vec4 base = texture2D(inputImageTexture, textureCoordinate);
//        lowp vec4 overlayer = texture2D(inputImageTexture2, textureCoordinate2);
//
//        gl_FragColor = overlayer * base + overlayer * (1.0 - base.a) + base * (1.0 - overlayer.a);
//    }
//);
#else
NSString *const kGPUImageOldMovieLookFragmentShaderString = SHADER_STRING
(
 varying vec2 textureCoordinate;
 varying vec2 textureCoordinate2;

 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;

 void main()
 {
     vec4 base = texture2D(inputImageTexture, textureCoordinate);
     vec4 overlayer = texture2D(inputImageTexture2, textureCoordinate2);

     gl_FragColor = overlayer * base + overlayer * (1.0 - base.a) + base * (1.0 - overlayer.a);
 }
);
#endif

@implementation GPUImageOldMovieLookFilter
{
    GPUImageOldMovieLookFilterMode _filterMode;
}

@synthesize overlayMovie = _overlayMovie;
@synthesize overlayPicture = _overlayPicture;

#pragma mark Lifecycle

- (id)init:(GPUImageOldMovieLookFilterMode)mode
{
    if (!(self = [super init]))
    {
        return nil;
    }
    
    _filterMode = mode;
    
    overlayFilter = [[GPUImageTwoInputFilter alloc] initWithFragmentShaderFromString: kGPUImageOldMovieLookFragmentShaderString];
    [self addFilter:overlayFilter];
    
    if (_filterMode == GPUImageOldMovieLookFilterModeMovie)
    {
        NSURL *overlayMovieURL = [[NSBundle mainBundle] URLForResource:@"old_movie_look" withExtension:@"mov"];
        
        _overlayMovie = [[THImageMovie alloc] initWithURL:overlayMovieURL];
        _overlayMovie.shouldRepeat = YES;
        
        [_overlayMovie addTarget:overlayFilter atTextureLocation:1];
    }
    else // if GPUImageOldMovieLookFilterModeImage
    {
        NSURL *overlayImageURL = [[NSBundle mainBundle] URLForResource:@"old_movie_look" withExtension:@"png"];
        
        _overlayPicture = [[GPUImagePicture alloc] initWithURL:overlayImageURL];
        
        [_overlayPicture addTarget:overlayFilter atTextureLocation:1];
    }
    
    self.initialFilters = @[overlayFilter];
    self.terminalFilter = overlayFilter;
    
    return self;
}

-(void)dealloc
{
    NSLog(@"GPUImageOldMovieLookFilter#dealloc");
}

#pragma mark Overridden methods

- (void)newFrameReadyAtTime:(CMTime)frameTime atIndex:(NSInteger)textureIndex;
{
    [super newFrameReadyAtTime:frameTime atIndex:textureIndex];

    if (_filterMode == GPUImageOldMovieLookFilterModeImage && textureIndex == 0)
    {
        [_overlayPicture processImage];
    }
}

#pragma mark Methods

- (NSArray<THImageMovie*>*) overlays
{
    return @[_overlayMovie];
}

@end