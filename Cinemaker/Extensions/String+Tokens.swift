//
//  String+Tokens.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 3/19/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

extension String
{
    func substitute(tokens:[String:String]) -> String
    {
        var string = self;
        
        for (key, value) in tokens
        {
            string = string.stringByReplacingOccurrencesOfString("{\(key)}", withString: value);
        }
        
        return string;
    }
}