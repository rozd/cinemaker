//
//  CAProgressLayer.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/8/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit

class CAProgressLayer: CALayer
{
    override class func needsDisplayForKey(key: String) -> Bool
    {
        if key == "progress" || key == "trackClor" || key == "progressColor"
        {
            return true;
        }
        
        return super.needsDisplayForKey(key);
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //--------------------------------------------------------------------------

    init(trackColor: CGColor, progressColor: CGColor)
    {
        self.trackColor = trackColor;
        self.progressColor = progressColor;
        
        super.init();
    }
    
    override init(layer: AnyObject)
    {
        let progressLayer = layer as! CAProgressLayer;
        
        self.progress = progressLayer.progress;
        self.trackColor = progressLayer.trackColor;
        self.progressColor = progressLayer.progressColor;
    
        super.init(layer: layer);
    }

    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //--------------------------------------------------------------------------
    
    var progress:CGFloat = 0.0
    {
        didSet
        {
            self.setNeedsDisplay();
        }
    }
    
    var trackColor: CGColor;
    
    var progressColor: CGColor;
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //--------------------------------------------------------------------------

    override func drawInContext(ctx: CGContext)
    {
        let totalWidth = CGRectGetWidth(self.bounds);
        let totalHeight = CGRectGetHeight(self.bounds);

        if self.progress > 0.0
        {
            CGContextSetFillColorWithColor(ctx, self.progressColor);
            CGContextFillRect(ctx, CGRectMake(0.0, 0.0, totalWidth * self.progress, totalHeight));
        }
        
        if self.progress < 1.0
        {
            CGContextSetFillColorWithColor(ctx, self.trackColor);
            CGContextFillRect(ctx, CGRectMake(totalWidth * self.progress, 0.0, totalWidth * (1 - self.progress), totalHeight));
        }
    }
}
