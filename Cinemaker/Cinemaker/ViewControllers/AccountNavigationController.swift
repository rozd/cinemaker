//
//  AccountNavigationController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/6/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit

class AccountNavigationController: UINavigationController
{
    //-----------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------------
    
    deinit
    {
        print("AccountNavigationController#deinit");
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //-----------------------------------------------------------------------------
    
    private var navigationBarHairlineImageView: UIImageView?;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //-----------------------------------------------------------------------------
    
    var viewModel: AccountViewModel!;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //-----------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad();

        self.navigationBarHairlineImageView = self.findHairlineImage(self.navigationBar);
        
        if let accountInfoViewController = self.topViewController as? AccountInfoViewController
        {
            accountInfoViewController.viewModel = self.viewModel.newAccountInfoViewModel();
        }
    }

    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated);
        
        self.navigationBarHairlineImageView?.hidden = true;
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning();
    }
    
    override func pushViewController(viewController: UIViewController, animated: Bool)
    {
        print("MoviewNavigationController#pushViewController:\(viewController)");
        
        if let accountSettingsViewController = viewController as? AccountSettingsViewController
        {
            accountSettingsViewController.viewModel = self.viewModel.newAccountSettingsViewModel();
        }
        
        super.pushViewController(viewController, animated: animated);
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Actions
    //
    //-----------------------------------------------------------------------------

    func findHairlineImage(view: UIView) -> UIImageView?
    {
        if view is UIImageView && CGRectGetHeight(view.bounds) <= 1.0
        {
            return view as? UIImageView;
        }
        
        for subview in view.subviews
        {
            if let hairlineImage = self.findHairlineImage(subview)
            {
                return hairlineImage;
            }
        }
        
        return nil;
    }

}
