//
//  SignInViewController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 3/6/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit
import TwitterKit

class SignInViewController: UIViewController, UITextViewDelegate, SignInViewModelDelegate
{
    //--------------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //--------------------------------------------------------------------------

    //-------------------------------------
    //  MARK: Outlets
    //-------------------------------------

    @IBOutlet weak var signInButton: TWTRLogInButton!
    @IBOutlet weak var guestTextView: UITextView!
    @IBOutlet weak var disclaimerTextView: UITextView!

    //-------------------------------------
    //  MARK: ViewModel
    //-------------------------------------

    var viewModel:SignInViewModel!;

    //--------------------------------------------------------------------------
    //
    //  MARK: - Overridden methods
    //
    //--------------------------------------------------------------------------

    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        self.viewModel.delegate = self;

        self.signInButton.logInCompletion = self.viewModel.loginButtonHandler;

        self.guestTextView.delegate = self;
        self.disclaimerTextView.delegate = self;

        let centeredParagraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle()
        centeredParagraphStyle.alignment = .Center;

        let guestString:NSMutableAttributedString = NSMutableAttributedString(string: "or continue as Guest",
            attributes:
            [
                NSFontAttributeName : UIFont(name: "Helvetica", size: 16)!,
                NSParagraphStyleAttributeName : centeredParagraphStyle
            ]);

        guestString.addAttributes(
            [
                NSForegroundColorAttributeName : Colors.grayDove
            ],
            range: NSRange(location: 0, length: 14));

        guestString.addAttributes(
            [
                NSForegroundColorAttributeName : Colors.blueIndigo,
                NSLinkAttributeName : NSURL(string: "cinemaker://guestlogin")!
            ],
            range: NSRange(location: 15, length: 5));

        self.guestTextView.attributedText = guestString;

        let disclaimerString:NSMutableAttributedString =
        NSMutableAttributedString(string: "By signing in here you agree to our\nTerms of Use and Privacy Policy",
            attributes:
            [
                NSParagraphStyleAttributeName : centeredParagraphStyle
            ]);

        disclaimerString.addAttributes(
            [
                NSForegroundColorAttributeName : Colors.grayTundora
            ],
            range: NSRange(location: 0, length: 35));

        disclaimerString.addAttributes(
            [
                NSForegroundColorAttributeName : Colors.blueIndigo,
                NSLinkAttributeName : NSURL(string: CinemakerTermsOfUseLink)!
            ],
            range: NSRange(location: 36, length: 12));
        
        disclaimerString.addAttributes(
            [
                NSForegroundColorAttributeName : Colors.grayTundora
            ],
            range: NSRange(location: 49, length: 3));

        disclaimerString.addAttributes(
            [
                NSForegroundColorAttributeName : Colors.blueIndigo,
                NSLinkAttributeName : NSURL(string: CinemakerPrivacyPolicyLink)!
            ],
            range: NSRange(location: 53, length: 14));


        self.disclaimerTextView.attributedText = disclaimerString;
    }

    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated);

        self.viewModel.reactivate();
    }

    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated)

        self.viewModel.deactivate();
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle
    {
        return .LightContent;
    }

    //--------------------------------------------------------------------------
    //
    //  MARK: - Methods
    //
    //--------------------------------------------------------------------------

    //-------------------------------------
    //  MARK: UITextViewDelegate
    //-------------------------------------

    func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool
    {
        if textView == self.disclaimerTextView
        {
            return true;
        }
        else
        {
            self.viewModel.loginAsGuest();
            
            return false;
        }
    }

    //-------------------------------------
    //  MARK: SignInViewModelDelegate
    //-------------------------------------

    func signInDidSuccess()
    {
        self.dismissViewControllerAnimated(false, completion: nil);

        if let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
        {
            appDelegate.showMainScreen();
        }
    }

    func signInDidFailure(error:NSError)
    {
        let errorAlertController = UIAlertController(title: "Error".localized, message: error.localizedDescription, preferredStyle: .Alert);

        errorAlertController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));

        self.presentViewController(errorAlertController, animated: true, completion: nil);
    }
}
