//
//  String+Localization.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/15/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

extension String
{
    var localized: String
    {
        return self.localizedWithComment("");
    }
    
    func localizedWithComment(comment: String) -> String
    {
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "", comment: comment);
    }
}