//
//  GPUImageLightLeakLookFilter.h
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/13/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GPUImage/GPUImageFramework.h>
#import "GPUImageLookFilter.h"

typedef NS_ENUM(NSInteger, GPUImageLightLeakLookFilterMode)
{
    GPUImageLightLeakLookFilterModeImage,
    GPUImageLightLeakLookFilterModeMovie,
};

@interface GPUImageLightLeakLookFilter : GPUImageFilterGroup <GPUImageLookFilter>
{
    GPUImageTwoInputFilter* overlayFilter;
}

- (id) init:(GPUImageLightLeakLookFilterMode) mode;

@property (readonly, nonatomic) THImageMovie* overlayMovie;
@property (readonly, nonatomic) GPUImagePicture *overlayPicture;

@end
