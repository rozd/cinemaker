//
//  Cinemaker.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 1/31/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import TwitterKit

let CinemakerSourceMoviePath = "\(NSTemporaryDirectory())source-movie.mp4";
let CinemakerOutputMoviePath = "\(NSTemporaryDirectory())output-movie.mp4";
let CinemakerTwitterUserName = "cinemakerapp";
let CinemakerTwitterCollection = "700081534271942656";
let CinemakerWebsiteLink = "http://www.cinemaker.tk";
let CinemakerInfoEmail = "cinemakerapp@gmail.com"
let CinemakerPrivacyEmail = "cinemakerapp@gmail.com";
let CinemakerTermsOfUseLink = "http://www.cinemaker.tk/terms";
let CinemakerPrivacyPolicyLink = "http://www.cinemaker.tk/privacy";
let CinemakerAPI = "https://app-cinemaker.rhcloud.com/api";

enum CinemakerNotification : String 
{
    case ReportTweetDidSuccess;
    case ReportTweetDidFailed;

    case ReportIssueDidSuccess;
    case ReportIssueDidFailed;

    case ReportDidTerminated;
}

class Cinemaker : NSObject, CinemakerServiceDelegate
{
    //-------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-------------------------------------------------------------------------

    override init()
    {
        super.init();
        
        self.cinemakerService = CinemakerService();
        self.cinemakerService.delegate = self;
    }

    //------------------------------------
    //  MARK: - Variables
    //------------------------------------

    // MARK: Models

    let account:TwitterAccount = TwitterAccount();

    // MARK: Services

    var cinemakerService:CinemakerService!;

    //-------------------------------------------------------------------------
    //
    //  MARK: - Methods
    //
    //-------------------------------------------------------------------------

    // MARK: Session
    
    func createNewMovieSession() -> MovieSession
    {
        return MovieSession(sourcePath: CinemakerSourceMoviePath, outputPath: CinemakerOutputMoviePath);
    }

    // MARK: Backend

    func tweetMovie(tweet:TWTRTweet)
    {
        self.cinemakerService.retweetTweet(tweet, completion: nil);
    }

    func reportTweet(tweet:TWTRTweet)
    {
        self.cinemakerService.reportTweet(tweet);
    }

    func reportIssue(subject:String, details:String?)
    {
        self.cinemakerService.reportIssue(subject, details: details);
    }

    //------------------------------------
    //  MARK: - Delegates
    //------------------------------------

    // MARK: CinemakerServiceDelegate

    func cinemakerServiceDidReportTweetSuccess()
    {
        NSNotificationCenter.defaultCenter().postNotificationName(CinemakerNotification.ReportTweetDidSuccess.rawValue, object: self);
    }

    func cinemakerServiceDidReportTweetFailed(error: NSError)
    {
        NSNotificationCenter.defaultCenter().postNotificationName(CinemakerNotification.ReportTweetDidFailed.rawValue, object: self);
    }

    func cinemakerServiceDidReportIssueSuccess()
    {
        NSNotificationCenter.defaultCenter().postNotificationName(CinemakerNotification.ReportIssueDidSuccess.rawValue, object: self);
    }

    func cinemakerServiceDidReportIssueFailed(error: NSError)
    {
        NSNotificationCenter.defaultCenter().postNotificationName(CinemakerNotification.ReportIssueDidFailed.rawValue, object: self);
    }

    func cinemakerServiceDidReportTerminated()
    {
        NSNotificationCenter.defaultCenter().postNotificationName(CinemakerNotification.ReportDidTerminated.rawValue, object: self);
    }

}