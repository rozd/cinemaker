//
//  AccountInfoViewModel.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/6/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import TwitterKit

protocol AccountInfoViewModelDelegate : class
{
    func accountInfoDidUserChange();
    func accountInfoDidErrorOccurs(error: NSError);
}

class AccountInfoViewModel : NSObject
{
    //-------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-------------------------------------------------------------------------
    
    init(account: TwitterAccount)
    {
        self.account = account;
    }
    
    deinit
    {
        print("AccountInfoViewModel#deinit");
    }
    
    //-------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //-------------------------------------------------------------------------
    
    let account:TwitterAccount;
    
    //-------------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //-------------------------------------------------------------------------
    
    //--------------------------------------
    //  MARK: Delegate
    //--------------------------------------

    weak var delegate:AccountInfoViewModelDelegate?;
    
    //--------------------------------------
    //  MARK: Current User
    //--------------------------------------
    
    var userScreenName: String?
    {
        return self.account.currentUser?.screenName ?? self.account.currentSession?.userName ?? "Anonymous".localized;
    }
    
    var userPictureURL: String?
    {
        return self.account.currentUser?.profileImageLargeURL;
    }

    //--------------------------------------
    //  MARK: Login handler
    //--------------------------------------

    var loginButtonHandler: (session:TWTRSession?, error:NSError?) -> ()
    {
        return self.account.loginButtonHandler;
    }

    //-------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-------------------------------------------------------------------------

    func reactivate()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AccountInfoViewModel.twitterUserChangeNotificationHandler(_:)), name: TwitterAccountNotification.TwitterUserChange.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AccountInfoViewModel.twitterErrorOccursNotificationHandler(_:)), name: TwitterAccountNotification.TwitterErrorOccurs.rawValue, object: nil);

        self.account.refresh();
    }

    func deactivate()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self);
    }
    
    //-------------------------------------------------------------------------
    //
    //  MARK: - Notification handlers
    //
    //-------------------------------------------------------------------------
    
    func twitterUserChangeNotificationHandler(notification: NSNotification)
    {
        self.delegate?.accountInfoDidUserChange();
    }
    
    func twitterErrorOccursNotificationHandler(notification: NSNotification)
    {
        self.delegate?.accountInfoDidErrorOccurs(notification.userInfo!["error"] as! NSError);
    }
}