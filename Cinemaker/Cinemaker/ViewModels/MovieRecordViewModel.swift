//
//  MovieRecordViewModel.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 1/31/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

protocol MovieRecordViewModelDelegate: class
{
    func recordViewModelInputDidRenew(input: AnyObject) // when camera or recorder change
    func recordViewModelInputDidUpdate();
    
    func recordViewModelRecordDidStart();
    func recordViewModelRecordProgress(progress: Float, minimumReached: Bool, maximumReached: Bool);
    func recordViewModelRecordDidFinish();
    func recordViewModelRecordDidFailed(error: NSError);
    func recordViewModelRecordDidTerminate();
    
    func recordViewModelGhostDidChange(active: Bool, snapshot: UIImage?);

    func recordViewModelShowInfo(message: String);
}

class MovieRecordViewModel: NSObject
{
    //-----------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------------
    
    init(session: MovieSession)
    {
        self.session = session;
        
        super.init();
    }
    
    deinit
    {
        print("MovieRecordViewModel.deinit");
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //-----------------------------------------------------------------------------

    private var session:MovieSession;

    private var progressTimer:NSTimer?;
    
    private var snapshotTimer:NSTimer?;
    private var snapshotTimerPauseDate: NSDate?;
    private var snapshotTimerPreviousFireDate: NSDate?;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //-----------------------------------------------------------------------------
    
    //--------------------------------------
    //  MARK: delegate
    //--------------------------------------
    
    weak var delegate: MovieRecordViewModelDelegate?;
    
    //--------------------------------------
    //  MARK: Camera properties
    //--------------------------------------
    
    var cameraInput: AnyObject
    {
        return self.session.recordService!.currentInput;
    }
    
    var cameraFullscreen: Bool
    {
        return self.session.recordService!.cameraFullscreen;
    }
    
    var cameraOrientation: UIInterfaceOrientation
    {
        return self.session.recordService!.cameraOrientation;
    }

    //--------------------------------------
    //  MARK: Ghost properties
    //--------------------------------------

    var ghostModeActive: Bool = false
    {
        didSet
        {
            self.delegate?.recordViewModelGhostDidChange(self.ghostModeActive, snapshot: self.ghostSnapshot);
        }
    }
    
    var ghostSnapshot: UIImage?
    {
        didSet
        {
            self.delegate?.recordViewModelGhostDidChange(self.ghostModeActive, snapshot: self.ghostSnapshot)
        }
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-----------------------------------------------------------------------------

    func reactivate()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieRecordViewModel.deviceOrientationDidChangeNotification(_:)), name: UIDeviceOrientationDidChangeNotification, object: nil);

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieRecordViewModel.applicationDidEnterBackgroundNotificationHandler(_:)), name: UIApplicationDidEnterBackgroundNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieRecordViewModel.applicationWillEnterForegroundNotificationHandler(_:)), name: UIApplicationWillEnterForegroundNotification, object: nil);

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieRecordViewModel.recordDidStartNotificationHandler(_:)), name: MovieSessionNotification.RecordDidStart.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieRecordViewModel.recordDidEndNotificationHandler(_:)), name: MovieSessionNotification.RecordDidEnd.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieRecordViewModel.recordDidFinishNotificationHandler(_:)), name: MovieSessionNotification.RecordDidFinish.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieRecordViewModel.recordDidFailedNotificationHandler(_:)), name: MovieSessionNotification.RecordDidFailed.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MovieRecordViewModel.recordDidTerminateNotificationHandler(_:)), name: MovieSessionNotification.RecordDidTerminated.rawValue, object: nil);

        self.session.reactivateRecord();
    }

    func deactivate()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self);

        self.session.deactivateRecord();
    }

    //--------------------------------------
    //  Methods: Camera
    //--------------------------------------
    
    func pauseCamera()
    {
        self.session.pauseCamera();
    }
    
    func resumeCamera()
    {
        Log("MovieRecordViewModel.resumeCamera()");

        self.session.resumeCamera();
    }
    
    //--------------------------------------
    //  Methods: Record
    //--------------------------------------
    
    func resumeRecord()
    {
        if self.session.recordService!.isFinished
        {
            self.delegate?.recordViewModelShowInfo("In current version you can't continue record after apply a Look.".localized);

            return;
        }

        self.session.resumeRecord();

        self.scheduleProgressTimer();
        self.scheduleSnapshotTimer();
    }
    
    func pauseRecord()
    {
        self.session.pauseRecord();

        self.teardownProgressTimer();
        self.teardownSnapshotTimer();

        self.ghostSnapshot = self.session.createSnapshot();
    }
    
    func finishRecrod()
    {
        self.session.finishRecord();

        self.teardownProgressTimer();
        self.teardownSnapshotTimer();
    }

    //--------------------------------------
    //  Methods: Settings
    //--------------------------------------
    
    func toggleCameraPosition()
    {
        self.session.toggleCameraPosition();
    }
    
    func toggleCameraFlash()
    {
        self.session.toggleCameraFlash();
    }
    
    func toggleCameraFullscreen()
    {
        self.session.toggleCameraFullscreen();
        
        self.delegate?.recordViewModelInputDidUpdate();
    }
    
    func toggleGhostMode()
    {
        Log("MovieRecordViewModel.toggleGhostMode()");

        self.ghostModeActive = !self.ghostModeActive;
    }
    
    func toggleGridMode()
    {
        // TODO implement
    }
    
    func toggleHorizonMode()
    {
        // TODO implement
    }

    //-------------------------------------
    //  MARK: update progress
    //-------------------------------------
    
    func scheduleProgressTimer()
    {
        print("scheduleProgressTimer");

        self.progressTimer = NSTimer.scheduledTimerWithTimeInterval(0.001, target: self, selector: #selector(MovieRecordViewModel.updateProgress), userInfo: nil, repeats: true);
    }
    
    func teardownProgressTimer()
    {
        print("teardownProgressTimer");

        self.progressTimer?.invalidate();
        self.progressTimer = nil;
    }
    
    func updateProgress()
    {
        let currentDuration:Float64 = CMTimeGetSeconds(self.session.recordService!.currentDuration);
        let maximumDuration:Float64 = CMTimeGetSeconds(self.session.maximumDuration);
        
        let progress:Float = Float(currentDuration / maximumDuration);
        
        let minimumReached:Bool = CMTimeCompare(self.session.recordService!.currentDuration, self.session.minimumDuration) >= 0;
        let maximumReached:Bool = CMTimeCompare(self.session.recordService!.currentDuration, self.session.maximumDuration) >= 0;
        
        if maximumReached
        {
            self.finishRecrod();
        }
        
        self.delegate?.recordViewModelRecordProgress(progress, minimumReached: minimumReached, maximumReached: maximumReached);
    }
    
    //----------------------------------
    //  MARK: snapshots
    //----------------------------------
    
    func scheduleSnapshotTimer()
    {
        print("scheduleSnapshotTimer");
        
        if let pauseTime = self.snapshotTimerPauseDate?.timeIntervalSinceNow
        {
            let fireDate = NSDate(timeInterval: -pauseTime, sinceDate: self.snapshotTimerPreviousFireDate!);
            
            self.snapshotTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(MovieRecordViewModel.createSnpashot), userInfo: nil, repeats: true);
            self.snapshotTimer?.fireDate = fireDate;
        }
        else
        {
            self.snapshotTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(MovieRecordViewModel.createSnpashot), userInfo: nil, repeats: true);
        }
    }
    
    func teardownSnapshotTimer()
    {
        print("teardownSnapshotTimer");
        
        self.snapshotTimerPauseDate = NSDate();
        self.snapshotTimerPreviousFireDate = self.snapshotTimer?.fireDate;
        
        self.snapshotTimer?.invalidate();
        self.snapshotTimer = nil;
    }
    
    func createSnpashot()
    {
        if let snapshot = self.session.createSnapshot()
        {
            self.session.snapshots.append(snapshot);
            
            let thumbnail = self.createThumbnail(snapshot)
            
            self.session.thumbnails.append(thumbnail);
        }
    }
    
    func createThumbnail(image: UIImage) -> UIImage
    {
        let k = 162 / min(image.size.width, image.size.height);
        
        let cgImage = image.CGImage
        
        let width = Int(CGFloat(CGImageGetWidth(cgImage)) * k);
        let height = Int(CGFloat(CGImageGetHeight(cgImage)) * k);
        let bitsPerComponent = CGImageGetBitsPerComponent(cgImage)
        let bytesPerRow = CGImageGetBytesPerRow(cgImage)
        let colorSpace = CGImageGetColorSpace(cgImage)
        let bitmapInfo = CGImageGetBitmapInfo(cgImage)
        
        let context = CGBitmapContextCreate(nil, width, height, bitsPerComponent, bytesPerRow, colorSpace, bitmapInfo.rawValue)
        
        CGContextSetInterpolationQuality(context, .High);
        
        CGContextDrawImage(context, CGRect(origin: CGPointZero, size: CGSize(width: CGFloat(width), height: CGFloat(height))), cgImage)
        
        let scaledImage = CGBitmapContextCreateImage(context).flatMap { UIImage(CGImage: $0) }
        
        return scaledImage!;
    }
    
    //----------------------------------
    //  MARK: Close
    //----------------------------------
    
    func attemptToClose(viewController: UIViewController, barButtonItem: UIBarButtonItem, callback: (interrupt: Bool) -> ())
    {
        Log("Attempt to dismiss MovieRecord");

        if self.session.outputAssetId != nil
        {
            callback(interrupt: false);
        }
        else if self.session.isOutputMovieCreated
        {
            Log("Propose to save Artwork");

            let saveOutputDialog = UIAlertController(title: nil, message: "You have unsaved artwork, do you want to save int to Camera Roll before exit?".localized, preferredStyle: .ActionSheet);
            saveOutputDialog.addAction(UIAlertAction(title: "Keep".localized, style: .Default, handler: { (action: UIAlertAction) -> Void in

                Log("Try to save Artwork to CameraRoll");

                self.session.saveToCameraRoll() { (assetId: String?, error: NSError?) -> () in
                    
                    if error == nil
                    {
                        Log("Artwork saved, MovieRecord can be dismissed");

                        callback(interrupt: false);
                    }
                    else
                    {
                        Log(error!.localizedDescription);

                        let saveErrorAlert = UIAlertController(title: "Error".localized, message: "Some error occured during saving movie, do you want to continue exit?".localized, preferredStyle: .Alert);
                        
                        viewController.presentViewController(saveErrorAlert, animated: true, completion: nil);
                        
                        callback(interrupt: true);
                    }
                };
            }));
            saveOutputDialog.addAction(UIAlertAction(title: "Discard".localized, style: .Destructive, handler: { (action: UIAlertAction) -> Void in

                Log("Dismiss MovieRecord w/o saving Artwork");

                callback(interrupt: false);
            }));
            saveOutputDialog.addAction(UIAlertAction(title: "Cancel".localized, style: .Cancel, handler: { (action: UIAlertAction) -> Void in

                callback(interrupt: true);
            }));

            saveOutputDialog.popoverPresentationController?.barButtonItem = barButtonItem;
            
            viewController.presentViewController(saveOutputDialog, animated: true, completion: nil);
        }
        else if self.session.isSourceMovieCreated
        {
            Log("Warn of recorded Movie");

            let guardSourceAlert = UIAlertController(title: nil, message: "You have recorded video that will be removed, do you relay want to coninue?".localized, preferredStyle: .ActionSheet);
            guardSourceAlert.addAction(UIAlertAction(title: "Continue".localized, style: .Destructive, handler: { (action: UIAlertAction) -> Void in
                
                callback(interrupt: false);
            }));
            guardSourceAlert.addAction(UIAlertAction(title: "Cancel".localized, style: .Cancel, handler: { (action: UIAlertAction) -> Void in
                
                callback(interrupt: true);
            }));

            guardSourceAlert.popoverPresentationController?.barButtonItem = barButtonItem;
            
            viewController.presentViewController(guardSourceAlert, animated: true, completion: nil);
        }
        else
        {
            callback(interrupt: false);
        }
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: - Notifications handlers
    //
    //-----------------------------------------------------------------------------

    // MARK: UIDevice

    func deviceOrientationDidChangeNotification(notification: NSNotification)
    {
        self.session.setDeviceOrientation(UIDevice.currentDevice().orientation);

        self.delegate?.recordViewModelInputDidUpdate();
    }

    //  MARK: UIApplication

    func applicationDidEnterBackgroundNotificationHandler(notification: NSNotification)
    {
        Log("MovieRecordViewModel.applicationDidEnterBackgroundNotificationHandler");

        self.pauseCamera();

        if self.session.recordService.isRecording
        {
            self.pauseRecord();
        }
    }

    func applicationWillEnterForegroundNotificationHandler(notification: NSNotification)
    {
        Log("MovieRecordViewModel.applicationWillEnterForegroundNotificationHandler");

        self.resumeCamera();
    }

    //  MARK: RecorderService

    func recordDidStartNotificationHandler(notification: NSNotification)
    {
        self.delegate?.recordViewModelRecordDidStart();
    }

    func recordDidEndNotificationHandler(notification: NSNotification)
    {
        self.teardownProgressTimer();
        self.teardownSnapshotTimer();
    }

    func recordDidFinishNotificationHandler(notification: NSNotification)
    {
        self.delegate?.recordViewModelRecordDidFinish();
    }
    
    func recordDidFailedNotificationHandler(notification: NSNotification)
    {
        self.delegate?.recordViewModelRecordDidFailed(NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: notification.userInfo));
    }

    func recordDidTerminateNotificationHandler(notification: NSNotification)
    {
        switch UIApplication.sharedApplication().applicationState
        {
            case .Active :

                self.delegate?.recordViewModelRecordDidFailed(NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: notification.userInfo));

            default :

                self.delegate?.recordViewModelRecordDidTerminate();
        }
    }
}