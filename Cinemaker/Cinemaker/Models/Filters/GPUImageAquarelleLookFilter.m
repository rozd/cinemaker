//
//  GPUImageAquarelleLookFilter.m
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/15/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

#import "GPUImageAquarelleLookFilter.h"

@implementation GPUImageAquarelleLookFilter

#pragma mark -
#pragma mark Lifecycle

-(id) init;
{
    if (!(self = [super init]))
    {
        return nil;
    }
    
    bilateralFilter = [[GPUImageBilateralFilter alloc] init];
    [self addFilter:bilateralFilter];
    
    kuwaharaFilter = [[GPUImageKuwaharaFilter alloc] init];
    [self addFilter:kuwaharaFilter];
    
    [bilateralFilter addTarget: kuwaharaFilter];
    
    self.initialFilters = @[bilateralFilter];
    self.terminalFilter = kuwaharaFilter;
    
    self.radius = 2;
    self.distanceNormalizationFactor = 2.0;
    
    return self;
}


#pragma mark -
#pragma mark Properties

@synthesize radius;
@synthesize distanceNormalizationFactor;

-(void) setRadius:(NSUInteger)newValue
{
    kuwaharaFilter.radius = newValue;
}

-(NSUInteger) radius
{
    return kuwaharaFilter.radius;
}

-(void) setDistanceNormalizationFactor:(CGFloat) newValue
{
    bilateralFilter.distanceNormalizationFactor = newValue;
}

-(CGFloat) distanceNormalizationFactor
{
    return bilateralFilter.distanceNormalizationFactor;
}

@end
