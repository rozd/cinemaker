//
//  AccountSettingsViewController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/6/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit
import MessageUI

class AccountSettingsViewController: UITableViewController, MFMailComposeViewControllerDelegate
{
    //-------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-------------------------------------------------------------------------
    
    deinit
    {
        print("AccountSettingsViewController#deinit");
    }
    
    //-------------------------------------------------------------------------
    //
    //  MARK: Outlets
    //
    //-------------------------------------------------------------------------
    
    @IBOutlet weak var termsOfUserCell: UITableViewCell!
    @IBOutlet weak var privacyPolicyCell: UITableViewCell!
    @IBOutlet weak var openSourceCell: UITableViewCell!
    @IBOutlet weak var appVersionCell: UITableViewCell!
    @IBOutlet weak var sendMessageCell: UITableViewCell!
    @IBOutlet weak var complainContentCell: UITableViewCell!
    @IBOutlet weak var logoutViewCell: UITableViewCell!
    
    //-------------------------------------------------------------------------
    //
    //  MARK: Proeprties
    //
    //-------------------------------------------------------------------------
    
    var viewModel: AccountSettingsViewModel!;
    
    //-------------------------------------------------------------------------
    //
    //  MARK: - Overridden methods
    //
    //-------------------------------------------------------------------------
    
    //------------------------------------
    //  UIViewController
    //------------------------------------

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @available(iOS 5.0, *)
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        super.prepareForSegue(segue, sender: sender);

        if let webViewController = segue.destinationViewController as? AccountWebViewController
        {
            if sender as! UITableViewCell == self.termsOfUserCell
            {
                webViewController.location = NSURL(string: CinemakerTermsOfUseLink);
            }
            else if sender as! UITableViewCell == self.privacyPolicyCell
            {
                webViewController.location = NSURL(string: CinemakerPrivacyPolicyLink);
            }
            else if sender as! UITableViewCell == self.openSourceCell
            {
                let open_source_libraries_html = NSBundle.mainBundle().pathForResource("open-source-libraries", ofType: "html")!;

                webViewController.location = NSURL(fileURLWithPath: open_source_libraries_html)
            }
        }
    }

    //------------------------------------
    //  UITableViewController
    //------------------------------------

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if let cell = tableView.cellForRowAtIndexPath(indexPath)
        {
            if cell == self.termsOfUserCell
            {
                self.performSegueWithIdentifier("ShowWebView", sender: self.termsOfUserCell);
            }
            else if cell == self.privacyPolicyCell
            {
                self.performSegueWithIdentifier("ShowWebView", sender: self.privacyPolicyCell);
            }
            else if cell == self.openSourceCell
            {
                self.performSegueWithIdentifier("ShowWebView", sender: self.openSourceCell);
            }
            else if cell == self.appVersionCell
            {
                self.viewModel.showAppVersion(self);
            }
            else if cell == self.sendMessageCell
            {
                self.viewModel.sendMessage(self);
            }
            else if cell == self.complainContentCell
            {
                self.viewModel.sendAbuse(self);
            }
            else if cell == self.logoutViewCell
            {
                self.viewModel.logout();

                if let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
                {
                    appDelegate.showSingInScreen(true);
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    //
    //  MARK: - Delegates
    //
    //-------------------------------------------------------------------------

    //-------------------------------------
    //  MFMailComposeViewControllerDelegate
    //-------------------------------------

    @available(iOS 3.0, *)
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?)
    {
        if result == MFMailComposeResultSent
        {
            self.dismissViewControllerAnimated(true, completion: nil);

            let infoDialog = UIAlertController(title: "Info".localized, message: "Your message was successfully sent.".localized, preferredStyle: .Alert);
            infoDialog.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));

            self.presentViewController(infoDialog, animated: true, completion: nil);
        }
        else if result == MFMailComposeResultSaved
        {
            self.dismissViewControllerAnimated(true, completion: nil);

            let infoDialog = UIAlertController(title: "Info".localized, message: "Your message saved as draft.".localized, preferredStyle: .Alert);
            infoDialog.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));

            self.presentViewController(infoDialog, animated: true, completion: nil);
        }
        else if result == MFMailComposeResultFailed
        {
            let infoDialog = UIAlertController(title: "Error".localized, message: error?.localizedDescription, preferredStyle: .Alert);
            infoDialog.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));

            self.presentingViewController?.presentViewController(infoDialog, animated: true, completion: nil);
        }
        else // if cancelled
        {
            self.dismissViewControllerAnimated(true, completion: nil);
        }
    }

}
