//
// Created by Max Rozdobudko on 3/11/16.
// Copyright (c) 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import TwitterKit

protocol TwitterServiceDelegate : class
{
    func twitterServiceMediaUploadDidStatus(status:TWTRMediaUploadStatus);
    func twitterServiceTweetDidSuccess(tweet:TWTRTweet);
    func twitterServiceTweetDidFaulire(error:NSError);
    func twitterServiceTweetDidTerminate();
    
    func twitterServiceUserBlockDidSuccess(screenName:String);
    func twitterServiceUserBlockDidFailure(error:NSError);
    func twitterServiceUserBlockDidTerminate();
}

class TwitterService
{
    //-------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-------------------------------------------------------------------------

    deinit
    {
        print("TwitterService.deinit");
    }

    //-------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //-------------------------------------------------------------------------

    private var backgroundTaskId:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid;

    private var isStatusUpdateRunning:Bool = false;
    private var isUserBlockRunning:Bool = false;
    
    //-------------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //-------------------------------------------------------------------------

    //  MARK: Delegate

    weak var delegate:TwitterServiceDelegate?;

    //-------------------------------------------------------------------------
    //
    //  MARK: - Methods
    //
    //-------------------------------------------------------------------------

    //  MARK: Status Update

    func tweetFrom(userID:String, status:String, mediaURL:NSURL)
    {
        self.beginBackgroundTask();
        
        self.isStatusUpdateRunning = true;

        let client = TWTRAPIClient(userID: userID);

        client.mediaUploadStatusCallback =
        {
            (status:TWTRMediaUploadStatus) in

            self.delegate?.twitterServiceMediaUploadDidStatus(status);
        }

        client.statusUpdate(status, mediaURL: mediaURL, mimeType: nil) { (tweet:TWTRTweet?, error:NSError?) -> () in

            dispatch_async(GlobalMainQueue)
            {
                self.endBackgroundTask();
                
                self.isStatusUpdateRunning = false;

                if let tweet = tweet
                {
                    self.delegate?.twitterServiceTweetDidSuccess(tweet);
                }
                else if let error = error
                {
                    self.delegate?.twitterServiceTweetDidFaulire(error);
                }
                else
                {
                    self.delegate?.twitterServiceTweetDidFaulire(NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey : "Unknown error during tweeting status update"]));
                }
            }
        }
    }
    
    // MARK: Block User
    
    func blockUser(userID:String, screenName:String)
    {
        let BLOCKS_CREATE_ENDPOINT:String = "https://api.twitter.com/1.1/blocks/create.json";
        
        let client = TWTRAPIClient(userID: userID);
        
        let params =
        [
            "screen_name" : screenName,
            "skip_status" : "1",
            "include_entities" : "0"
        ];
        
        var clientError:NSError?;
        let request = client.URLRequestWithMethod("POST", URL: BLOCKS_CREATE_ENDPOINT, parameters: params, error: &clientError);
        
        if clientError == nil
        {
            self.beginBackgroundTask();
            
            self.isUserBlockRunning = true;

            client.sendTwitterRequest(request)
            {
                (response:NSURLResponse?, responseData:NSData?, responseError:NSError?) -> Void in
                
                self.endBackgroundTask();
                
                self.isUserBlockRunning = false;
                
                if responseError == nil
                {
                    self.delegate?.twitterServiceUserBlockDidSuccess(screenName);
                }
                else
                {
                    self.delegate?.twitterServiceUserBlockDidFailure(responseError!);
                }
            };
        }
        else
        {
            self.delegate?.twitterServiceUserBlockDidFailure(clientError!);
        }
    }
    
    private func terminateCurrentRequest()
    {
        if self.isStatusUpdateRunning
        {
            self.delegate?.twitterServiceTweetDidTerminate();
        }
        
        if self.isUserBlockRunning
        {
            self.delegate?.twitterServiceUserBlockDidTerminate();
        }
        
        self.isStatusUpdateRunning = false;
        self.isUserBlockRunning = false;
    }
    
    // MARK: Background

    func beginBackgroundTask()
    {
        if self.backgroundTaskId == UIBackgroundTaskInvalid
        {
            self.backgroundTaskId = UIApplication.sharedApplication().beginBackgroundTaskWithName("TwitterService.backgroundTask")
            { [unowned self] in

                self.terminateCurrentRequest();

                self.endBackgroundTask();
            }
        }

    }

    func endBackgroundTask()
    {
        if self.backgroundTaskId != UIBackgroundTaskInvalid
        {
            UIApplication.sharedApplication().endBackgroundTask(self.backgroundTaskId);

            self.backgroundTaskId = UIBackgroundTaskInvalid;
        }
    }
}
