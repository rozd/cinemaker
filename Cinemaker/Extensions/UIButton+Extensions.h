//
// Created by Max Rozdobudko on 1/18/16.
// Copyright (c) 2016 Max Rozdobudko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIButton (Extensions)

@property(nonatomic, assign) UIEdgeInsets hitTestEdgeInsets;

@end