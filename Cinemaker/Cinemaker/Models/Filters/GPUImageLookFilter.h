//
//  GPUImageLookFilter.h
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/13/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

#ifndef GPUImageLookFilter_h
#define GPUImageLookFilter_h

#import <Foundation/Foundation.h>

@class THImageMovie;

@protocol GPUImageLookFilter <NSObject>

@property (readonly, nonatomic) NSArray<THImageMovie*>* overlays;

@end


#endif /* GPUImageLookFilter_h */
