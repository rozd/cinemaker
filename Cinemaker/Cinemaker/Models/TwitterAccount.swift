//
//  Account.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/7/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import TwitterKit

enum TwitterAccountNotification : String
{
    case TwitterUserChange;
    case TwitterErrorOccurs;
    case TwitterLoginSuccess;
    case TwitterLoginCancel;

    case TweetDidStatus;
    case TweetDidSuccess;
    case TweetDidFailed;
    case TweetDidTerminate;
    
    case UserBlockDidSuccess;
    case UserBlockDidFailed;
    case UserBlockDidTerminate;
}

class TwitterAccount : TwitterServiceDelegate
{
    //-------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-------------------------------------------------------------------------

    init()
    {
        self.twitterService = TwitterService();
        self.twitterService.delegate = self;
    }

    deinit
    {
        print("TwitterAccount.deinit");
    }

    //-------------------------------------------------------------------------
    //
    //  MARK: - Variables
    //
    //-------------------------------------------------------------------------

    let loggedInAsKey:String = "loggedInAsKey";

    //-------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //-------------------------------------------------------------------------

    let twitterService:TwitterService;
    
    //-------------------------------------
    //  currentUser
    //-------------------------------------
    
    private var _currentUser: TWTRUser?
    
    var currentUser:TWTRUser?
    {
        return self._currentUser;
    }
    
    //-------------------------------------
    //  currentUser
    //-------------------------------------
    
    var currentSession:TWTRSession?
    {
        return Twitter.sharedInstance().sessionStore.session() as? TWTRSession;
    }
    
    //-------------------------------------------------------------------------
    //
    //  MARK: - Methods
    //
    //-------------------------------------------------------------------------

    //------------------------------------
    //  MARK: Authentication
    //------------------------------------
    
    func isLoggedIn() -> Bool
    {
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults();

        return prefs.stringForKey(self.loggedInAsKey) != nil;
    }
    
    func refresh()
    {
        Log("TwitterAccount.refresh()");

        if let session = Twitter.sharedInstance().sessionStore.session()
        {
            self.loadUserWithID(session.userID);
        }
        else
        {
            NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterUserChange.rawValue, object: self);
        }
    }

    func loginIfNeeded()
    {
        Log("TwitterAccount.loginIfNeeded");

        if let session = Twitter.sharedInstance().sessionStore.session()
        {
            NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterLoginSuccess.rawValue, object: self);

            self.loadUserWithID(session.userID);
        }
        else
        {
            Twitter.sharedInstance().logInWithCompletion()
            {
                [unowned self] (session: TWTRSession?, error: NSError?) -> Void in

                if let userId = session?.userID
                {
                    self.storeLoggedInAsUser();

                    NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterLoginSuccess.rawValue, object: self);

                    self.loadUserWithID(userId);
                }
                else if error?.code == TWTRLogInErrorCode.Canceled.rawValue
                {
                    NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterLoginCancel.rawValue, object: self);
                }
                else
                {
                    NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterErrorOccurs.rawValue, object: self, userInfo: ["error" : error!]);
                }
            }
        }
    }

    func loginAsGuest()
    {
        Log("TwitterAccount.loginAsGuest()");

        self.storeLoggedInAsGuest();

        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterLoginSuccess.rawValue, object: self);
    }

    func loginButtonHandler(session:TWTRSession?, error:NSError?)
    {
        Log("TwitterAccount.loginButtonHandler()");

        if error == nil
        {
            self.storeLoggedInAsUser();

            NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterLoginSuccess.rawValue, object: self);

            self.loadUserWithID(session!.userID);
        }
        else if error!.code == TWTRLogInErrorCode.Canceled.rawValue
        {
            NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterLoginCancel.rawValue, object: self);
        }
        else
        {
            NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterErrorOccurs.rawValue, object: self, userInfo: ["error": error!]);
        }
    }

    func logout()
    {
        Log("TwitterAccount.logout()");

        let domainName = NSBundle.mainBundle().bundleIdentifier;
        NSUserDefaults.standardUserDefaults().removePersistentDomainForName(domainName!);
        
        if let userID = Twitter.sharedInstance().sessionStore.session()?.userID
        {
            Twitter.sharedInstance().sessionStore.logOutUserID(userID);
        }
        
        self._currentUser = nil;
        
        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterUserChange.rawValue, object: self);
    }

    private func loadUserWithID(userID: String)
    {
        let client = TWTRAPIClient(userID: userID);

        client.loadUserWithID(userID)
        {
            [unowned self] (user: TWTRUser?, error: NSError?) -> Void in

            if error == nil
            {
                self._currentUser = user;

                NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterUserChange.rawValue, object: self);
            }
            else
            {
                NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterErrorOccurs.rawValue, object: self, userInfo: ["error" : error!]);
            }
        }
    }

    //  MARK: API

    func updateStatus(status:String, mediaURL:NSURL)
    {
        if let userID = Twitter.sharedInstance().sessionStore.session()?.userID
        {
            self.twitterService.tweetFrom(userID, status: status, mediaURL: mediaURL);
        }
        else
        {
            Twitter.sharedInstance().logInWithCompletion { [unowned self] (session:TWTRSession?, error:NSError?) -> Void in

                dispatch_async(GlobalMainQueue)
                {
                    if let userID = session?.userID
                    {
                        self.twitterService.tweetFrom(userID, status: status, mediaURL: mediaURL);
                    }
                    else if let error = error
                    {
                        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterErrorOccurs.rawValue, object: self, userInfo: ["error" : error]);
                    }
                    else
                    {
                        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TwitterErrorOccurs.rawValue, object: self, userInfo: ["error" : NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey : "Could not login to Twitter"])]);
                    }
                }
            }
        }
    }
    
    func blockUser(screenName:String)
    {
        if let userID = Twitter.sharedInstance().sessionStore.session()?.userID
        {
            self.twitterService.blockUser(userID, screenName: screenName);
        }
        else
        {
            Twitter.sharedInstance().logInWithCompletion { [unowned self] (session:TWTRSession?, error:NSError?) -> Void in
                
                dispatch_async(GlobalMainQueue)
                {
                    if let userID = session?.userID
                    {
                        self.twitterService.blockUser(userID, screenName: screenName);
                    }
                    else if let error = error
                    {
                        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.UserBlockDidFailed.rawValue, object: self, userInfo: ["error" : error]);
                    }
                    else
                    {
                        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.UserBlockDidFailed.rawValue, object: self, userInfo: ["error" : NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey : "Could not login to Twitter"])]);
                    }
                }
            }
        }
    }

    //  MARK: Working with User Defaults

    private func storeLoggedInAsUser()
    {
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults();
        prefs.setObject("user", forKey: self.loggedInAsKey);

        prefs.synchronize();
    }

    private func storeLoggedInAsGuest()
    {
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults();
        prefs.setObject("guest", forKey: self.loggedInAsKey);

        prefs.synchronize();
    }

    //-------------------------------------------------------------------------
    //
    //  MARK: - Delegates
    //
    //-------------------------------------------------------------------------

    //  MARK: TwitterServiceDelegate

    func twitterServiceMediaUploadDidStatus(status:TWTRMediaUploadStatus)
    {
        var message:String?;

        Log(status.toString());

        switch status
        {
            case .Init :
                message = "Initializing Session";

            case .Inited :
                message = "Session Iinitialized";

            case .Appended :
                message = "Data transfered";

            case .SegmentAppend(_, let segmentIndex, let remainingSegmentCount) :
                message = "Transfering data\n(Part \(segmentIndex + 1) of \(segmentIndex + remainingSegmentCount + 1))";

            case .Finalize :
                message = "Finalizing Movie";

            case .Finalized :
                message = "Movie finalized";
            
            default :
                message = nil;
        }

        if let message = message
        {
            NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TweetDidStatus.rawValue, object: self, userInfo: ["value" : message]);
        }
    }

    func twitterServiceTweetDidSuccess(tweet:TWTRTweet)
    {
        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TweetDidSuccess.rawValue, object: self, userInfo: ["value" : tweet]);
    }

    func twitterServiceTweetDidFaulire(error:NSError)
    {
        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TweetDidFailed.rawValue, object: self, userInfo: ["error" : error]);
    }

    func twitterServiceTweetDidTerminate()
    {
        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.TweetDidTerminate.rawValue, object: self, userInfo: nil);
    }
    
    func twitterServiceUserBlockDidSuccess(screenName:String)
    {
        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.UserBlockDidSuccess.rawValue, object: self, userInfo: ["value" : screenName]);
    }
    
    func twitterServiceUserBlockDidFailure(error:NSError)
    {
        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.UserBlockDidFailed.rawValue, object: self, userInfo: ["error" : error]);
    }
    
    func twitterServiceUserBlockDidTerminate()
    {
        NSNotificationCenter.defaultCenter().postNotificationName(TwitterAccountNotification.UserBlockDidTerminate.rawValue, object: self, userInfo: nil);
    }
}