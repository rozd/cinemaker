//
//  UIGridView.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/4/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit

class UIGridView: UIView
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //--------------------------------------------------------------------------
    
    var rowCount: Int = 2;
    
    var columnCount: Int = 2;

    //--------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //--------------------------------------------------------------------------
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        let context = UIGraphicsGetCurrentContext();
        CGContextSetLineWidth(context, 0.5);
        CGContextSetStrokeColorWithColor(context, UIColor.whiteColor().CGColor);
    
        let columnWidht = self.frame.size.width / CGFloat(self.columnCount + 1);
        
        for i in 1...self.columnCount
        {
            let startPoint = CGPointMake(columnWidht * CGFloat(i), 0.0);
            let endPoint = CGPointMake(startPoint.x, self.frame.size.height);
            
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
            CGContextStrokePath(context);
        }
        
        let rowHeight = self.frame.size.height / CGFloat(self.rowCount + 1);
        
        for j in 1...self.rowCount
        {
            let startPoint = CGPointMake(0.0, rowHeight * CGFloat(j));
            let endPoint = CGPointMake(self.frame.size.width, startPoint.y);
            
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
            CGContextStrokePath(context);
        }
    }

}
