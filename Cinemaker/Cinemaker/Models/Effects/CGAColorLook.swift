//
//  CGAColorLook.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/13/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

class CGAColorLook : Look
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecyle
    //
    //--------------------------------------------------------------------------
    
    init()
    {
        print("CGAColorLook#init");
    }
    
    deinit
    {
        print("CGAColorsLook#init");
    }
    
    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------
    
    var name: String
    {
        return "CGA Monitor";
    }
    
    var filterForPreview:GPUImageOutput = GPUImageCGAColorspaceFilter()
    
    var filterForThumbnail:GPUImageOutput = GPUImageCGAColorspaceFilter();
    
    var filterForMovie:GPUImageOutput
    {
        return GPUImageCGAColorspaceFilter();
    }
}