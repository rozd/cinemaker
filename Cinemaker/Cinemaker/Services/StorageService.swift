//
//  StorageService.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/3/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import Photos

class StorageService
{
    func saveToCameraRoll(movieURL: NSURL, withCompletion completion: (assetId: String?, error: NSError?) -> ())
    {
        var assetIdentifier:String?;
        
        PHPhotoLibrary.sharedPhotoLibrary().performChanges(
        {
            let request = PHAssetChangeRequest.creationRequestForAssetFromVideoAtFileURL(movieURL);
            let assetPlaceholder = request?.placeholderForCreatedAsset;
            
            assetIdentifier = assetPlaceholder?.localIdentifier;
        },
        completionHandler: { (success:Bool, error:NSError?) in
            
            dispatch_async(GlobalMainQueue)
            {
                if error == nil
                {
                    if assetIdentifier != nil
                    {
                        completion(assetId: assetIdentifier, error: nil);
                        
//                    let result = PHAsset.fetchAssetsWithLocalIdentifiers([assetIdentifier!], options: nil);
//
//                    if let asset = result.firstObject as? PHAsset
//                    {
//                        PHImageManager.defaultManager().requestAVAssetForVideo(asset, options: nil) { (avAsset:AVAsset?, avAudio:AVAudioMix?, info:[NSObject : AnyObject]?) -> Void in
//
//                            if let urlAsset = avAsset as? AVURLAsset
//                            {
//
//                            }
//                            else
//                            {
//                                print(avAsset);
//                                print(info);
//                            }
//
//                        };
//                    }
//                    else
//                    {
//                        callback(assetURL: nil, error: NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: ["detail" : "Could not retrieve movie asset."]));
//                    }
                    }
                    else
                    {
                        completion(assetId: nil, error: NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: ["detail" : "Could not obtain local identifier for movie asset."]));
                    }
                }
                else
                {
                    completion(assetId: nil, error: error);
                }
            }
        })
    }
}