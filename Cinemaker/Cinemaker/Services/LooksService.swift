//
//  LooksService.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/4/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

protocol LooksServiceDelegate: class
{
    func looksServiceDidFinish();
    func looksSerivceDidFailed(error: NSError);
    func looksServiceDidTerminate();
}

class LooksService
{
    //-----------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------------
    
    deinit
    {
        print("LooksService.deinit");
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //-----------------------------------------------------------------------------
    
    private var inputMovieTH:THImageMovie!;
    private var movieWriterTH:THImageMovieWriter!;
    
    private var processingOutput: GPUImageOutput!;
    private var processingMovies: [THImageMovie] = [];
    
    private var backgroundTaskId:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //-----------------------------------------------------------------------------
    
    //--------------------------------------
    //  delegate
    //--------------------------------------
    
    weak var delegate: LooksServiceDelegate?;
    
    //--------------------------------------
    //  currentProgress
    //--------------------------------------
    
    var currentProgress:Float
    {
        if self.inputMovieTH != nil && !self.inputMovieTH!.progress.isNaN
        {
            return self.inputMovieTH!.progress;
        }
        else
        {
            return 0.0;
        }
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-----------------------------------------------------------------------------
    
    func applyLook(look: Look, inputMovieURL: NSURL, outputMovieURL: NSURL)
    {
        self.beginBackgroundTask();
        
        self.processingOutput = look.filterForMovie;
        
        // movies
        
        let inputMovieAsset:AVAsset = AVAsset(URL: inputMovieURL);
        let inputFirstTrack:AVAssetTrack = inputMovieAsset.tracks[0];
        
        let outputVideoSize:CGSize = inputFirstTrack.naturalSize;
        
        self.processingOutput.forceProcessingAtSize(outputVideoSize);
        
        self.inputMovieTH = THImageMovie(asset: inputMovieAsset);
        self.inputMovieTH!.addTarget(self.processingOutput as! GPUImageInput);
        
        self.processingMovies.append(self.inputMovieTH);
        
        if let lookFilter = self.processingOutput as? GPUImageLookFilter
        {
            self.processingMovies.appendContentsOf(lookFilter.overlays);
        }
        
        // writer
        
        unlink(outputMovieURL.fileSystemRepresentation);
        
//        self.movieWriterTH = THImageMovieWriter(movieURL: outputMovieURL, size: outputVideoSize, movies: self.processingMovies);
        self.movieWriterTH = THImageMovieWriter(movieURL: outputMovieURL, size: outputVideoSize, fileType: AVFileTypeMPEG4, outputSettings: self.bestQualityH264(outputVideoSize), movies: self.processingMovies);
//        self.movieWriterTH!.shouldPassthroughAudio = true;
        
        self.processingOutput.addTarget(self.movieWriterTH!);
        
        for processingMovie in self.processingMovies
        {
            processingMovie.startProcessing();
        }
        
        self.movieWriterTH!.startRecording();
        
        self.movieWriterTH!.completionBlock =
        {
            for processingMovie in self.processingMovies
            {
                processingMovie.endProcessing();
            }
            
            self.processingMovies.removeAll();
            
            self.inputMovieTH.removeTarget(self.processingOutput as! GPUImageInput);
            
            self.movieWriterTH!.finishRecording();
            
            self.inputMovieTH = nil;
            self.movieWriterTH = nil;
            
            let processingInput = self.processingOutput as! GPUImageInput;
            
            processingInput.endProcessing();
            
            dispatch_async(GlobalMainQueue)
            {
                self.delegate?.looksServiceDidFinish();
            }

            self.endBackgroundTask();
        }
        
        self.movieWriterTH!.failureBlock = { (error: NSError!) in
            
            for processingMovie in self.processingMovies
            {
                processingMovie.cancelProcessing();
            }

            self.processingMovies.removeAll();
            
            self.inputMovieTH.removeTarget(self.processingOutput as! GPUImageInput);

            self.movieWriterTH!.cancelRecording();
            
            self.inputMovieTH = nil;
            self.movieWriterTH = nil;
            
            let processingInput = self.processingOutput as! GPUImageInput;
            
            processingInput.endProcessing();

            dispatch_async(GlobalMainQueue)
            {
                self.delegate?.looksSerivceDidFailed(error);
            }
            
            self.endBackgroundTask();
        }
    }
    
    private func terminateLook()
    {
        for processingMovie in self.processingMovies
        {
            processingMovie.cancelProcessing();
        }
        
        self.processingMovies.removeAll();
        
        self.inputMovieTH.removeTarget(self.processingOutput as! GPUImageInput);
        
        self.movieWriterTH.cancelRecording();
        
        self.inputMovieTH = nil;
        self.movieWriterTH = nil;
        
        let processingInput = self.processingOutput as! GPUImageInput;
        
        processingInput.endProcessing();
        
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.looksServiceDidTerminate();
        }
    }
    
    //--------------------------------------
    //  MARK: Background
    //--------------------------------------
    
    func beginBackgroundTask()
    {
        if self.backgroundTaskId == UIBackgroundTaskInvalid
        {
            self.backgroundTaskId = UIApplication.sharedApplication().beginBackgroundTaskWithName("LooksService.backgroundTask")
            { [unowned self] in
                
                self.terminateLook();
                
                self.endBackgroundTask();
            }
        }
        
    }
    
    func endBackgroundTask()
    {
        if self.backgroundTaskId != UIBackgroundTaskInvalid
        {
            UIApplication.sharedApplication().endBackgroundTask(self.backgroundTaskId);
            
            self.backgroundTaskId = UIBackgroundTaskInvalid;
        }
    }

    // MARK: Quality settings
    
    func bestQualityH264(size: CGSize) -> [String : AnyObject]
    {
        print(AVVideoPixelAspectRatioHorizontalSpacingKey);
        
        let settings:[String:AnyObject] =
        [
            AVVideoCodecKey : AVVideoCodecH264,
            AVVideoWidthKey : size.width,
            AVVideoHeightKey : size.height,
//            AVVideoAverageNonDroppableFrameRateKey : 24,
            
            AVVideoPixelAspectRatioKey :
            [
                AVVideoPixelAspectRatioHorizontalSpacingKey : 1,
                AVVideoPixelAspectRatioVerticalSpacingKey : 1
            ],
            
            AVVideoCompressionPropertiesKey :
            [
                AVVideoProfileLevelKey : AVVideoProfileLevelH264High41,
                AVVideoMaxKeyFrameIntervalKey : 24,
                AVVideoAverageBitRateKey : 10000000, // 10Mbps
                
//                AVVideoMaxKeyFrameIntervalDurationKey : 0.0,
//                AVVideoH264EntropyModeKey : AVVideoH264EntropyModeCABAC,
            ]
        ]
        
        return settings;
    }
}