//
//  AcidPartyLook.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/14/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit

class AcidPartyLook : Look
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecyle
    //
    //--------------------------------------------------------------------------
    
    init()
    {
        print("PartyDrunkLook#init");
    }
    
    deinit
    {
        print("AcidPartyLook#init");
    }
    
    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------
    
    var name: String
    {
        return "Acid Party";
    }
    
    var filterForPreview:GPUImageOutput = GPUImageAcidPartyLookFilter(.Image);
    
    var filterForThumbnail:GPUImageOutput = GPUImageAcidPartyLookFilter(.Image);
    
    var filterForMovie:GPUImageOutput
    {
        return GPUImageAcidPartyLookFilter(.Movie);
    }
}