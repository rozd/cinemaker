//
// Created by Max Rozdobudko on 12/29/15.
// Copyright (c) 2015 Max Rozdobudko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GPUImage/GPUImageFramework.h>
#import "GPUImageLookFilter.h"

@class THImageMovie;
@class GPUImagePicture;

typedef NS_ENUM(NSInteger, GPUImageOldMovieLookFilterMode)
{
    GPUImageOldMovieLookFilterModeImage,
    GPUImageOldMovieLookFilterModeMovie,
};

@interface GPUImageOldMovieLookFilter : GPUImageFilterGroup <GPUImageLookFilter>
{
//    GLint exposureUniform;
    
    GPUImageTwoInputFilter* overlayFilter;
}

- (id) init:(GPUImageOldMovieLookFilterMode) mode;

// Exposure ranges from -10.0 to 10.0, with 0.0 as the normal level
//@property(readwrite, nonatomic) CGFloat exposure;

@property (readonly, nonatomic) THImageMovie* overlayMovie;
@property (readonly, nonatomic) GPUImagePicture *overlayPicture;

@end