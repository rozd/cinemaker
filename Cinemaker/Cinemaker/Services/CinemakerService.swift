//
//  CinemakerService.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/18/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import TwitterKit

protocol CinemakerServiceDelegate : class
{
    func cinemakerServiceDidReportTweetSuccess();
    func cinemakerServiceDidReportTweetFailed(error:NSError);

    func cinemakerServiceDidReportIssueSuccess();
    func cinemakerServiceDidReportIssueFailed(error:NSError);

    func cinemakerServiceDidReportTerminated();
}

class CinemakerService
{
    //--------------------------------------
    //  MARK: - Lifecycle
    //--------------------------------------
    
    init()
    {
        print("CinemakerService.init()");
    }
    
    deinit
    {
        print("CinemakerService.deinit");
    }
    
    //--------------------------------------
    //  MARK: - Variables
    //--------------------------------------

    private var backgroundTaskId:UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid;

    //--------------------------------------
    //  MARK: - Propertes
    //--------------------------------------

    weak var delegate:CinemakerServiceDelegate?;

    //--------------------------------------
    //  MARK: - Twitter
    //--------------------------------------
    
    func retweetTweet(tweet: TWTRTweet, completion: ((data: NSData?, error: NSError?) -> ())?)
    {
        let params =
        [
            "tweetId" : tweet.tweetID
        ];
        
        var json:NSData? = nil;
        
        do
        {
            json = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted);
        }
        catch
        {
            return;
        }
        
        self.beginBackgroundTask();
        
        let request = NSMutableURLRequest(URL: NSURL(string: "\(CinemakerAPI)/twitter/retweet")!);
        request.HTTPMethod = "POST";

        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type");
        request.HTTPBody = json;
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            self.endBackgroundTask();

        }
        
        task.resume();
    }

    //--------------------------------------
    //  MARK: - Reports
    //--------------------------------------

    func reportTweet(tweet:TWTRTweet)
    {
        let tweetLink = tweet.retweetedTweet?.permalink.absoluteString ?? "ID:\(tweet.retweetedTweet?.tweetID)";
        
        let params =
        [
            "tweetId" : tweet.tweetID,
            "tweetLink" : tweetLink
        ];

        var json:NSData? = nil;

        do
        {
            json = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted);
        }
        catch let error as NSError
        {
            self.delegate?.cinemakerServiceDidReportTweetFailed(error);
            return;
        }
        catch
        {
            self.delegate?.cinemakerServiceDidReportTweetFailed(NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey : "Could not JSON encode Tweet data."]));
            return;
        }

        self.beginBackgroundTask();

        let request = NSMutableURLRequest(URL: NSURL(string: "\(CinemakerAPI)/reports/tweet")!);
        request.HTTPMethod = "POST";
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type");
        request.HTTPBody = json;

        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in

            self.endBackgroundTask();
            
            print(response);

            if error == nil
            {
                self.delegate?.cinemakerServiceDidReportTweetSuccess();
            }
            else
            {
                self.delegate?.cinemakerServiceDidReportTweetFailed(error!);
            }
        }

        task.resume();
    }

    func reportIssue(subject:String, details:String?)
    {
        let params =
        [
            "subject" : subject,
            "details" : details ?? ""
        ];

        var json:NSData? = nil;

        do
        {
            json = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted);
        }
        catch let error as NSError
        {
            self.delegate?.cinemakerServiceDidReportIssueFailed(error);
            return;
        }
        catch
        {
            self.delegate?.cinemakerServiceDidReportIssueFailed(NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey : "Could not JSON encode data."]));
            return;
        }

        self.beginBackgroundTask();

        let request = NSMutableURLRequest(URL: NSURL(string: "\(CinemakerAPI)/reports/issue")!);
        request.HTTPMethod = "POST";
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type");
        request.HTTPBody = json;

        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in

            self.endBackgroundTask();

            if error == nil
            {
                self.delegate?.cinemakerServiceDidReportIssueSuccess();
            }
            else
            {
                self.delegate?.cinemakerServiceDidReportIssueFailed(error!);
            }
        }

        task.resume();
    }

    //--------------------------------------
    //  MARK: Background
    //--------------------------------------
    
    func beginBackgroundTask()
    {
        if self.backgroundTaskId == UIBackgroundTaskInvalid
        {
            self.backgroundTaskId = UIApplication.sharedApplication().beginBackgroundTaskWithName("CinemakerService.backgroundTask")
            { [unowned self] in

                self.delegate?.cinemakerServiceDidReportTerminated();

                self.endBackgroundTask();
            }
        }
        
    }
    
    func endBackgroundTask()
    {
        if self.backgroundTaskId != UIBackgroundTaskInvalid
        {
            UIApplication.sharedApplication().endBackgroundTask(self.backgroundTaskId);
            
            self.backgroundTaskId = UIBackgroundTaskInvalid;
        }
    }
}