//
// Created by Max Rozdobudko on 1/9/16.
// Copyright (c) 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import GPUImage

class OldMovieLook : Look
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecyle
    //
    //--------------------------------------------------------------------------
    
    init()
    {
        print("OldMovieLook#init");
    }
    
    deinit
    {
        print("OldMovieLook#init");
    }
    
    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------
    
    var name: String
    {
        return "Old Movie";
    }
    
    var filterForPreview:GPUImageOutput = GPUImageOldMovieLookFilter(.Image);

    var filterForThumbnail:GPUImageOutput = GPUImageOldMovieLookFilter(.Image);

    var filterForMovie:GPUImageOutput
    {
        return GPUImageOldMovieLookFilter(.Movie);
    }
}
