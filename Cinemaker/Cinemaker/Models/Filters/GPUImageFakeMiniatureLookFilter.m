//
//  GPUImageFakeMiniatureFilter.m
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/16/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

#import "GPUImageFakeMiniatureLookFilter.h"

@implementation GPUImageFakeMiniatureLookFilter

-(id) init
{
    if (!(self = [super init]))
    {
        return nil;
    }
    
//    medianFilter = [[GPUImageMedianFilter alloc] init];
//    [self addFilter:medianFilter];
    
//    sharpenFilter = [[GPUImageSharpenFilter alloc] init];
//    sharpenFilter.sharpness = 1.0;
//    [self addFilter:sharpenFilter];
    
    tiltShiftFilter = [[GPUImageTiltShiftFilter alloc] init];
    tiltShiftFilter.blurRadiusInPixels = 15;
    tiltShiftFilter.focusFallOffRate = 0.4;
    tiltShiftFilter.topFocusLevel = 0.5;
    tiltShiftFilter.bottomFocusLevel = 0.5;
    [self addFilter:tiltShiftFilter];
//    
//    [medianFilter addTarget:sharpenFilter];
//    [sharpenFilter addTarget:tiltShiftFilter];
    
//    [medianFilter addTarget:tiltShiftFilter];
    
//    self.initialFilters = @[medianFilter, sharpenFilter];
    self.initialFilters = @[tiltShiftFilter];
    self.terminalFilter = tiltShiftFilter;
    
    return self;
}

-(void)dealloc
{
    NSLog(@"GPUImageFakeMiniatureLookFilter#dealloc");
}

@end
