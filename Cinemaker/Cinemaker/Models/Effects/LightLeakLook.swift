//
//  LightLeakLook.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/13/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

class LightLeakLook: Look
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecyle
    //
    //--------------------------------------------------------------------------
    
    init()
    {
        print("WarmLightLook#init");
    }
    
    deinit
    {
        print("WarmLightLook#init");
    }
    
    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------
    
    var name: String
    {
        return "Light Leak";
    }
    
    var filterForPreview:GPUImageOutput = GPUImageLightLeakLookFilter(.Image);
    
    var filterForThumbnail:GPUImageOutput = GPUImageLightLeakLookFilter(.Image);
    
    var filterForMovie:GPUImageOutput
    {
        return GPUImageLightLeakLookFilter(.Movie);
    }
}
