//
//  AccountWebViewController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/6/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit

class AccountWebViewController: UIViewController
{
    //-------------------------------------------------------------------------
    //
    //  MARK: - Variables
    //
    //-------------------------------------------------------------------------

    // MARK: Outlets

    @IBOutlet weak var webView:UIWebView!;

    //-------------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //-------------------------------------------------------------------------

    var location:NSURL?;

    //-------------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //-------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad();

        if let currentLocation = self.location
        {
            let request = NSURLRequest(URL: currentLocation);

            self.webView.loadRequest(request);
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
