//
//  MovieNavigationControllerViewController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/2/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit

class MovieNavigationController: UINavigationController
{
    //-----------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------------
    
    deinit
    {
        print("MovieNavigationController.deinit");
        
        self.viewModel.teardown();
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //-----------------------------------------------------------------------------
    
    var viewModel:MovieViewModel!;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //-----------------------------------------------------------------------------
    
    //--------------------------------------
    //  MARK: UIViewController
    //--------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        if let recordViewController = self.topViewController as? RecordingViewController
        {
            recordViewController.viewModel = self.viewModel.newRecordViewModel();
        }
    }
    
    override func viewDidDisappear(animated: Bool)
    {
        super.viewDidDisappear(animated);
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning();
    }
    
    //--------------------------------------
    //  MARK: UINavigationController
    //--------------------------------------
    
    override func pushViewController(viewController: UIViewController, animated: Bool)
    {
        print("MoviewNavigationController.pushViewController");
        
        if let movieLooksViewController = viewController as? MovieLooksViewController
        {
            movieLooksViewController.viewModel = self.viewModel.newLooksViewModel();
        }
        else if let movieShareViewController = viewController as? ShareViewController
        {
            movieShareViewController.viewModel = self.viewModel.newShareViewModel();
        }
        
        super.pushViewController(viewController, animated: animated);
    }
}
