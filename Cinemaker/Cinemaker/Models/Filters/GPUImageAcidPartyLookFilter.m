//
//  GPUImageAcidPartyLookFilter.m
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/15/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

#import "GPUImageAcidPartyLookFilter.h"

@implementation GPUImageAcidPartyLookFilter
{
    GPUImageAcidPartyLookFilterMode _filterMode;
}

@synthesize overlayMovie = _overlayMovie;
@synthesize overlayPicture = _overlayPicture;

#pragma mark Lifecycle

- (id)init:(GPUImageAcidPartyLookFilterMode) mode
{
    if (!(self = [super init]))
    {
        return nil;
    }
    
    _filterMode = mode;
    
    lowpassFilter = [[GPUImageLowPassFilter alloc] init];
    [self addFilter:lowpassFilter];
    
    overlayFilter = [[GPUImageScreenBlendFilter alloc] init];
    [self addFilter:overlayFilter];
    
    if (_filterMode == GPUImageAcidPartyLookFilterModeMovie)
    {
        NSURL *overlayMovieURL = [[NSBundle mainBundle] URLForResource:@"acid_party_look" withExtension:@"mov"];
        
        _overlayMovie = [[THImageMovie alloc] initWithURL:overlayMovieURL];
        _overlayMovie.shouldRepeat = YES;
        
        [_overlayMovie addTarget:overlayFilter atTextureLocation:1];
    }
    else
    {
        NSURL *overlayImageURL = [[NSBundle mainBundle] URLForResource:@"acid_party_look" withExtension:@"png"];
        
        _overlayPicture = [[GPUImagePicture alloc] initWithURL:overlayImageURL];
        
        [_overlayPicture addTarget:overlayFilter atTextureLocation:1];
    }
    
    [lowpassFilter addTarget:overlayFilter];
    
    self.initialFilters = @[lowpassFilter];
    self.terminalFilter = overlayFilter;
    
    return self;
}

-(void)dealloc
{
    NSLog(@"GPUImageAcidPartyLookFilter#dealloc");
}

#pragma mark -
#pragma mark Overridden methods

- (void)newFrameReadyAtTime:(CMTime)frameTime atIndex:(NSInteger)textureIndex;
{
    [super newFrameReadyAtTime:frameTime atIndex:textureIndex];
    
    if (_filterMode == GPUImageAcidPartyLookFilterModeImage && textureIndex == 0)
    {
        [_overlayPicture processImage];
    }
}

#pragma mark -
#pragma mark GPUImageLookFilter

- (NSArray<THImageMovie*>*) overlays
{
    return @[_overlayMovie];
}

@end
