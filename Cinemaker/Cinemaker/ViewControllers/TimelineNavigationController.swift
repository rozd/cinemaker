//
//  TimelineNavigationController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/18/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit

class TimelineNavigationController: UINavigationController
{
    //-----------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------------
    
    deinit
    {
        print("TimelineNavigationController#deinit");
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //-----------------------------------------------------------------------------
    
    var viewModel: TimelineViewModel!;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //-----------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        if let timelineViewController = self.topViewController as? TimelineViewController
        {
            timelineViewController.viewModel = self.viewModel;
        }
    }
}
