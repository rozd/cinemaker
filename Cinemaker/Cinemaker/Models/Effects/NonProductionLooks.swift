//
// Created by Max Rozdobudko on 1/9/16.
// Copyright (c) 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import GPUImage

class GrayscaleLook : Look
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecyle
    //
    //--------------------------------------------------------------------------
    
    init()
    {
        print("GrayscaleLook#init");
    }
    
    deinit
    {
        print("GrayscaleLook#init");
    }
    
    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------
    
    var name: String {
        return "Grayscale";
    }
    
    var filterForPreview:GPUImageOutput = GPUImageGrayscaleFilter();

    var filterForThumbnail:GPUImageOutput = GPUImageGrayscaleFilter();

    var filterForMovie:GPUImageOutput = GPUImageGrayscaleFilter();
}

class PixelateLook : Look
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecyle
    //
    //--------------------------------------------------------------------------

    init()
    {
        print("PixelateLook#init");
    }
    
    deinit
    {
        print("PixelateLook#init");
    }

    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------

    var name: String {
        return "Pixelate";
    }

    var filterForPreview:GPUImageOutput = GPUImagePixellateFilter();

    var filterForThumbnail:GPUImageOutput = GPUImagePixellateFilter();

    var filterForMovie:GPUImageOutput = GPUImagePixellateFilter();
}

class PolkadotLook: Look
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecyle
    //
    //--------------------------------------------------------------------------

    init()
    {
        print("PolkadotLook#init");
    }
    
    deinit
    {
        print("PolkadotLook#init");
    }

    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------

    var name: String {
        return "Polkadot";
    }

    var filterForPreview:GPUImageOutput = GPUImagePolkaDotFilter();

    var filterForThumbnail:GPUImageOutput = GPUImagePolkaDotFilter();

    var filterForMovie:GPUImageOutput = GPUImagePolkaDotFilter();
}

class InversionLook: Look
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecyle
    //
    //--------------------------------------------------------------------------

    init()
    {
        print("InversionLook#init");
    }
    
    deinit
    {
        print("InversionLook#init");
    }

    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------

    var name: String {
        return "Inversion";
    }

    var filterForPreview:GPUImageOutput = GPUImageColorInvertFilter();

    var filterForThumbnail:GPUImageOutput = GPUImageColorInvertFilter();

    var filterForMovie:GPUImageOutput
    {
        return GPUImageColorInvertFilter();
    }
}

class MissEtikateLook : Look
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecyle
    //
    //--------------------------------------------------------------------------
    
    init()
    {
        print("MissEtikateLook#init");
    }
    
    deinit
    {
        print("MissEtikateLook#init");
    }
    
    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------
    
    var name: String {
        return "Miss Etikate";
    }
    
    var filterForPreview:GPUImageOutput = GPUImageMissEtikateFilter();
    
    var filterForThumbnail:GPUImageOutput = GPUImageMissEtikateFilter();
    
    var filterForMovie:GPUImageOutput
    {
        return GPUImageMissEtikateFilter();
    }
}
