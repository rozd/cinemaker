//
//  AccountViewModel.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/6/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

class AccountViewModel
{
    //-----------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------------
    
    init(cinemaker: Cinemaker)
    {
        self.cinemaker = cinemaker;
    }
    
    deinit
    {
        print("AccountViewModel#deinit");
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //-----------------------------------------------------------------------------
    
    var cinemaker: Cinemaker;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-----------------------------------------------------------------------------
    
    func newAccountInfoViewModel() -> AccountInfoViewModel
    {
        return AccountInfoViewModel(account: self.cinemaker.account);
    }
    
    func newAccountSettingsViewModel() -> AccountSettingsViewModel
    {
        return AccountSettingsViewModel(account: self.cinemaker.account);
    }
}