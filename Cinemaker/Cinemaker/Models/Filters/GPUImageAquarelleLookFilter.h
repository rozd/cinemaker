//
//  GPUImageAquarelleLookFilter.h
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/15/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

#import <GPUImage/GPUImageFramework.h>

@interface GPUImageAquarelleLookFilter : GPUImageFilterGroup
{
    GPUImageBilateralFilter* bilateralFilter;
    GPUImageKuwaharaFilter* kuwaharaFilter;
}

/// The radius to sample from when creating the brush-stroke effect, with a default of 3. The larger the radius, the slower the filter.
@property(readwrite, nonatomic) NSUInteger radius;

// A normalization factor for the distance between central color and sample color.
@property(nonatomic, readwrite) CGFloat distanceNormalizationFactor;

@end
