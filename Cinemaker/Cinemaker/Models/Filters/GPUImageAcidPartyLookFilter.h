//
//  GPUImageAcidPartyLookFilter.h
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/15/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

#import <GPUImage/GPUImageFramework.h>
#import "GPUImageLookFilter.h"

typedef NS_ENUM(NSInteger, GPUImageAcidPartyLookFilterMode)
{
    GPUImageAcidPartyLookFilterModeImage,
    GPUImageAcidPartyLookFilterModeMovie,
};

@interface GPUImageAcidPartyLookFilter : GPUImageFilterGroup <GPUImageLookFilter>
{
    GPUImageTwoInputFilter* overlayFilter;
    GPUImageLowPassFilter* lowpassFilter;
}

- (id) init:(GPUImageAcidPartyLookFilterMode) mode;

@property (readonly, nonatomic) THImageMovie* overlayMovie;
@property (readonly, nonatomic) GPUImagePicture* overlayPicture;

@end
