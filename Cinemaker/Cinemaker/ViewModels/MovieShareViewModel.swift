
//
//  MovieShareViewModel.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/3/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import UIKit
import TwitterKit

protocol SharingViewModelDelegate : class
{
    func sharingViewModelDidSavingToCameraRollComplete();
    func sharingViewModelDidSavingToCameraRollFailed(error: NSError);
    func sharingViewModelDidOpenInstagramError(error: NSError);
}

class MovieShareViewModel
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //--------------------------------------------------------------------------
    
    init(cinemaker:Cinemaker, session: MovieSession, account:TwitterAccount)
    {
        self.cinemaker = cinemaker;
        self.session = session;
        self.account = account;
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //--------------------------------------------------------------------------

    let cinemaker:Cinemaker;

    let session:MovieSession;

    let account:TwitterAccount;
    
    //--------------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //--------------------------------------------------------------------------

    //------------------------------------
    //  MARK: delegate
    //------------------------------------

    weak var delegate: SharingViewModelDelegate?;

    //------------------------------------
    //  MARK: outputMovieURL
    //------------------------------------

    var outputMovieURL: NSURL
    {
        return self.session.outputMovieURL;
    }

    //------------------------------------
    //  MARK: twitterCollectionId
    //------------------------------------

    var twitterCollectionId: String
    {
        return CinemakerTwitterCollection;
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //--------------------------------------------------------------------------

    func newTweetViewModel() -> TweetViewModel
    {
        return TweetViewModel(account: self.account);
    }

    //--------------------------------------
    //  Methods: Sharing
    //--------------------------------------
    
    func shareOnInstagram()
    {
        SVProgressHUD.show();
        
        self.session.saveToCameraRoll { (assetId: String?, error: NSError?) -> () in
            
            SVProgressHUD.dismiss();
            
            if error == nil
            {
                let index = assetId!.startIndex.advancedBy(36);
                let id = assetId!.substringToIndex(index);
                
                let assetsLibraryPath = "assets-library://asset/asset.mp4?id=\(id)&ext=mp4";
                
                let escapedPath = assetsLibraryPath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())!;
                let escapedHastag = "#CNMKR".stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())!;
                
                if let openInstagramLibrary = NSURL(string: "instagram://library?AssetPath=\(escapedPath)&InstagramCaption=\(escapedHastag)")
                {
                    print(openInstagramLibrary);
                    
                    if (UIApplication.sharedApplication().canOpenURL(openInstagramLibrary))
                    {
                        UIApplication.sharedApplication().openURL(openInstagramLibrary);
                    }
                    else
                    {
                        self.delegate?.sharingViewModelDidOpenInstagramError(NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey : "Can't open Instagram applicaion.".localized]));
                    }
                }
                else
                {
                    self.delegate?.sharingViewModelDidOpenInstagramError(NSError(domain: "CinemakerErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey : "Oops, something wrong.".localized]));
                }
            }
            else
            {
                self.delegate?.sharingViewModelDidSavingToCameraRollFailed(error!);
            }
        }
    }
    
    func saveToCameraRoll()
    {
        SVProgressHUD.show();
        
        self.session.saveToCameraRoll { (assetId: String?, error: NSError?) -> () in
            
            dispatch_async(GlobalMainQueue)
            {
                if error == nil
                {
                    SVProgressHUD.showSuccessWithStatus("Done".localized);
                    
                    self.delegate?.sharingViewModelDidSavingToCameraRollComplete();
                }
                else
                {
                    SVProgressHUD.dismiss();
                    
                    self.delegate?.sharingViewModelDidSavingToCameraRollFailed(error!);
                }
            }
        }
    }
    
    func shareOnTwitter(viewController: UIViewController, sender: AnyObject)
    {
        do
        {
            let attributes = try NSFileManager.defaultManager().attributesOfItemAtPath(self.session.outputMovieURL.path!);
            
            if let fileSize: UInt64 = attributes[NSFileSize] as? UInt64
            {
                if fileSize > 15728640 // 15Mb
                {
                    let errorController = UIAlertController(title: "Error".localized, message: "Your video exceeds Twitter's limit in 15Mb".localized, preferredStyle: .Alert);
                    errorController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));

                    viewController.presentViewController(errorController, animated: true, completion: nil);
                    
                    return;
                }
            }
            
        }
        catch let error as NSError
        {
            let errorController = UIAlertController(title: "Error".localized, message: error.localizedDescription, preferredStyle: .Alert);
            errorController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));
            
            viewController.presentViewController(errorController, animated: true, completion: nil);
            
            return;
        }
        catch
        {
            let errorController = UIAlertController(title: "Error".localized, message: "Unknown error".localized, preferredStyle: .Alert);
            errorController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));

            viewController.presentViewController(errorController, animated: true, completion: nil);

            return;
        }
        
        viewController.performSegueWithIdentifier("ShowCreateTweet", sender: sender);
    }
    
    //--------------------------------------
    //  Methods: Twitter
    //--------------------------------------
    
    func retweetTweetToCinemaker(tweet: TWTRTweet)
    {
        self.cinemaker.tweetMovie(tweet);
    }
    
    //--------------------------------------
    //  Methods: Done
    //--------------------------------------
    
    func attemptToDone(viewController: UIViewController, barButtonItem: UIBarButtonItem, callback: (interrupt: Bool) -> ())
    {
        Log("Attempt to dismiss MovieShare");

        if self.session.outputAssetId != nil
        {
            callback(interrupt: false);
        }
        else if self.session.isOutputMovieCreated
        {
            Log("Propose to save Artwork");

            let saveOutputDialog = UIAlertController(title: nil, message: "You have unsaved artwork that would be discarded, do you want to save it to Camera Roll before exit?".localized, preferredStyle: .ActionSheet);
            
            saveOutputDialog.addAction(UIAlertAction(title: "Save".localized, style: .Default, handler: { (action: UIAlertAction) -> Void in

                Log("Try to save Artwork to CameraRoll");

                SVProgressHUD.show();
                
                self.session.saveToCameraRoll() { (assetId: String?, error: NSError?) -> () in

                    if error == nil
                    {
                        Log("Artwork saved, MovieShare can be dismissed");

                        SVProgressHUD.showSuccessWithStatus("Done".localized);
                        
                        callback(interrupt: false);
                    }
                    else
                    {
                        SVProgressHUD.dismiss();

                        Log(error!.localizedDescription);

                        let saveErrorAlert = UIAlertController(title: "Error".localized, message: error!.localizedDescription, preferredStyle: .Alert);

                        viewController.presentViewController(saveErrorAlert, animated: true, completion: nil);

                        callback(interrupt: true);
                    }
                };
            }));
            saveOutputDialog.addAction(UIAlertAction(title: "Discard".localized, style: .Destructive, handler: { (action: UIAlertAction) -> Void in

                Log("Dismiss MovieShare w/o saving Artwork");

                callback(interrupt: false);
            }));
            saveOutputDialog.addAction(UIAlertAction(title: "Cancel".localized, style: .Cancel, handler: { (action: UIAlertAction) -> Void in
                
                callback(interrupt: true);
            }));

            saveOutputDialog.popoverPresentationController?.barButtonItem = barButtonItem;
            
            viewController.presentViewController(saveOutputDialog, animated: true, completion: nil);
        }
        else if self.session.isSourceMovieCreated
        {
            Log("Warn of recorded Movie");

            let guardSourceAlert = UIAlertController(title: nil, message: "You have recorded video that will be removed, do you relay want to coninue?".localized, preferredStyle: .ActionSheet);
            guardSourceAlert.addAction(UIAlertAction(title: "Continue".localized, style: .Destructive, handler: { (action: UIAlertAction) -> Void in
                
                callback(interrupt: false);
            }));
            guardSourceAlert.addAction(UIAlertAction(title: "Cancel".localized, style: .Cancel, handler: { (action: UIAlertAction) -> Void in
                
                callback(interrupt: true);
            }));

            guardSourceAlert.popoverPresentationController?.barButtonItem = barButtonItem;
            
            viewController.presentViewController(guardSourceAlert, animated: true, completion: nil);
        }
        else
        {
            callback(interrupt: false);
        }
    }

}