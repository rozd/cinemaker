//
// Created by Max Rozdobudko on 1/9/16.
// Copyright (c) 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import GPUImage

protocol Look
{
    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------
    
    var name: String { get };

    var filterForPreview:GPUImageOutput { get };

    var filterForThumbnail:GPUImageOutput { get };

    var filterForMovie:GPUImageOutput { get };

//    var currentProgress:Float { get };

    //--------------------------------------------------------------------------
    //
    //  Methods
    //
    //--------------------------------------------------------------------------

//    func apply(inputMovieURL:NSURL, outputMovieURL:NSURL, handler:()->());
//
//    func apply(movie:GPUImageMovie, output:NSURL, handler:()->());
//
//    func cancel();
}
