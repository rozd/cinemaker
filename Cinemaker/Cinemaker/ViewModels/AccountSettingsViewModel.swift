//
//  AccountSettingsViewModel.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/6/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import UIKit
import MessageUI
import TwitterKit

class AccountSettingsViewModel
{
    //-----------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------------
    
    init(account: TwitterAccount)
    {
        self.account = account;
    }
    
    deinit
    {
        print("AccountSettingsViewModel#deinit");
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-----------------------------------------------------------------------------
    
    let account: TwitterAccount;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-----------------------------------------------------------------------------
    
    func logout()
    {
        self.account.logout();
    }

    func showAppVersion(viewController:UIViewController)
    {
        let versionString = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String;
        
        let versionDialog = UIAlertController(title: "Cinemaker", message: versionString, preferredStyle: .Alert);
        versionDialog.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));
        
        viewController.presentViewController(versionDialog, animated: true, completion: nil);
    }

    func sendMessage(viewController:UIViewController)
    {
        if MFMailComposeViewController.canSendMail()
        {
            let mailViewController = MFMailComposeViewController();
            mailViewController.mailComposeDelegate = viewController as? MFMailComposeViewControllerDelegate;

            mailViewController.setToRecipients([CinemakerInfoEmail]);

            viewController.presentViewController(mailViewController, animated: true, completion: nil);
        }
        else
        {
            let errorDialog = UIAlertController(title: "Error".localized, message: "Mail services are not available.", preferredStyle: .Alert);
            errorDialog.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));

            viewController.presentViewController(errorDialog, animated: true, completion: nil);
        }
    }

    func sendAbuse(viewController:UIViewController)
    {
        let infoDialog = UIAlertController(title: "Info".localized, message: "If you find any content that you think is inappropriate, please copy its link using share option on the Tweet and paste it to message composer showing next.".localized, preferredStyle: .Alert);
        infoDialog.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: { (action: UIAlertAction) -> Void in
            
            if MFMailComposeViewController.canSendMail()
            {
                let mailViewController = MFMailComposeViewController();
                mailViewController.mailComposeDelegate = viewController as? MFMailComposeViewControllerDelegate;
                
                mailViewController.setToRecipients([CinemakerPrivacyEmail]);
                mailViewController.setSubject("Inappropriate content found in Cinemaker iOS application");
                
                if let copiedText = UIPasteboard.generalPasteboard().string
                {
                    mailViewController.setMessageBody(copiedText, isHTML: false);
                }
                
                viewController.presentViewController(mailViewController, animated: true, completion: nil);
            }
            else
            {
                let errorDialog = UIAlertController(title: "Error".localized, message: "Mail services are not available.", preferredStyle: .Alert);
                errorDialog.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));
                
                viewController.presentViewController(errorDialog, animated: true, completion: nil);
            }
            
        }));
        
        viewController.presentViewController(infoDialog, animated: true, completion: nil);
    }
}