//
//  AppDelegate.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 12/18/15.
//  Copyright © 2015 Max Rozdobudko. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import TwitterKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    //-------------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //-------------------------------------------------------------------------
    
    var window: UIWindow?
    
    var cinemaker:Cinemaker!;
    var viewModel:MainViewModel!;

    //-------------------------------------------------------------------------
    //
    //  MARK: - Methods
    //
    //-------------------------------------------------------------------------
    
    //------------------------------------
    //  MARK: UIApplicationDelegate
    //------------------------------------

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
//        Twitter.sharedInstance().startWithConsumerKey("5d1fb7be33dd8ad517c10582bf7def8a347ca0b5", consumerSecret: "4611adb5412e32bfbf3c0bd1ae13fcc7749ce5a56e3d1f6a883cab96a46f6126");
        
//        Twitter.sharedInstance().startWithConsumerKey("mFIKZAuH9UcXaFRTrNsVhityL", consumerSecret: "Vdxqq3C9EE6ZRSRt2BT0snNCnAZYIhu7Xjr140N7QLkARAOgdQ");
        
        // setup Fabric
        
        Twitter.sharedInstance().startWithConsumerKey("5F1a2qGjzveIp5H3jvSaofaYz", consumerSecret: "31R3P39rupcJu1dOVZBLWhrs5k1mx243rZkrcCWfUjHAJUzaPm");

        Fabric.with([Crashlytics.self, Twitter.sharedInstance()]);
        
        // setup MVVM
        
        self.cinemaker = Cinemaker();
        self.viewModel = MainViewModel(cinemaker: cinemaker);
        
        let mainViewController = self.window?.rootViewController as! MainViewController;
        mainViewController.viewModel = viewModel;
        
        // check if logged in
        
        if !cinemaker.account.isLoggedIn()
        {
            self.showSingInScreen(false);
        }
        
        return true;
    }

    func applicationWillResignActive(application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication)
    {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication)
    {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    //------------------------------------
    //  MARK: Authentication
    //------------------------------------
    
    func showSingInScreen(animated:Bool)
    {
        let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil);
        
        let signInViewController = storyboard.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController;
        signInViewController.viewModel = self.viewModel.newSignInViewModel();
        
        self.window?.rootViewController = signInViewController;
    }
    
    func showMainScreen()
    {
        let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil);

        let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
        mainViewController.viewModel = self.viewModel;

        self.window?.rootViewController = mainViewController;
    }
}

