//
//  MainViewModel.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/1/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

class MainViewModel
{
    //-------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-------------------------------------------------------------------------

    init(cinemaker:Cinemaker)
    {
        self.cinemaker = cinemaker;
    }
    
    //-------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //-------------------------------------------------------------------------

    let cinemaker:Cinemaker;
    
    //-------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-------------------------------------------------------------------------
    
    func newTimelineViewModel() -> TimelineViewModel
    {
        return TimelineViewModel(cinemaker:self.cinemaker, account: self.cinemaker.account);
    }
    
    func newMovieViewModel() -> MovieViewModel
    {
        return MovieViewModel(cinemaker: self.cinemaker);
    }
    
    func newAccountViewModel() -> AccountViewModel
    {
        return AccountViewModel(cinemaker: self.cinemaker);
    }
    
    func newSignInViewModel() -> SignInViewModel
    {
        return SignInViewModel(account: self.cinemaker.account);
    }
}