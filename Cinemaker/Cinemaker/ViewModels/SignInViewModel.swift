//
//  SignInViewModel.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 3/6/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import TwitterKit

protocol SignInViewModelDelegate : class
{
    func signInDidSuccess();
    func signInDidFailure(error:NSError);
}

class SignInViewModel : NSObject
{
    //-------------------------------------------------------------------------
    //
    //  MARK: - Lifecycle
    //
    //-------------------------------------------------------------------------

    init(account:TwitterAccount)
    {
        self.account = account;
    }

    deinit
    {
        print("SignInViewModel.deinit");
    }

    //-------------------------------------------------------------------------
    //
    //  MARK: - Variables
    //
    //-------------------------------------------------------------------------

    let account:TwitterAccount;

    //-------------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //-------------------------------------------------------------------------

    //------------------------------------
    //  MARK: Delegate
    //------------------------------------

    weak var delegate:SignInViewModelDelegate?;

    //------------------------------------
    //  MARK: Login handler
    //------------------------------------

    var loginButtonHandler:(session:TWTRSession?, error:NSError?) -> ()
    {
        return self.account.loginButtonHandler;
    }

    //-------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-------------------------------------------------------------------------

    func reactivate()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SignInViewModel.twitterUserLoginNotificationHandler(_:)), name: TwitterAccountNotification.TwitterLoginSuccess.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SignInViewModel.twitterErrorOccursNotificationHandler(_:)), name: TwitterAccountNotification.TwitterErrorOccurs.rawValue, object: nil);

        self.account.refresh();
    }

    func deactivate()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self);
    }

    func loginAsGuest()
    {
        self.account.loginAsGuest();
    }

    //-------------------------------------------------------------------------
    //
    //  MARK: Notification handlers
    //
    //-------------------------------------------------------------------------

    func twitterUserLoginNotificationHandler(notification: NSNotification)
    {
        self.delegate?.signInDidSuccess();
    }

    func twitterErrorOccursNotificationHandler(notification: NSNotification)
    {
        self.delegate?.signInDidFailure(notification.userInfo!["error"] as! NSError);
    }
}