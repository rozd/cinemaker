//
//  MovieLooksViewController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/2/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit

class MovieLooksViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, MovieLooksViewModelDelegate
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //--------------------------------------------------------------------------
    
    deinit
    {
        print("MovieLooksViewController.deinit");
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //--------------------------------------------------------------------------
    
    //-------------------------------------
    //  Variables: Outlets
    //-------------------------------------
    
    @IBOutlet weak var applyBarButtonItem: UIBarButtonItem!;
    
    @IBOutlet weak var previewView: GPUImageView!

    @IBOutlet weak var previewPageControl: UIPageControl!
    
    @IBOutlet weak var looksCollectionView: UICollectionView!
    
    @IBOutlet weak var previewViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var previewViewWidthConstraint: NSLayoutConstraint!
    
    //-------------------------------------
    //  Variables: Preview
    //-------------------------------------
    
    var currentPreview: GPUImageOutput?;
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //--------------------------------------------------------------------------
    
    var viewModel: MovieLooksViewModel!;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //-----------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad();

        self.viewModel.delegate = self;

        self.previewViewWidthConstraint.constant = CGRectGetWidth(self.view.bounds);
        self.previewViewHeightConstraint.constant = CGRectGetWidth(self.view.bounds);
        self.view.layoutIfNeeded();

        self.applyBarButtonItem.enabled = false;

        self.previewPageControl.numberOfPages = self.viewModel.totalPreviewCount;
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated);

        self.viewModel.reactivate();
    }

    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated);
        
        self.viewModel.deactivate();
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //--------------------------------------------------------------------------
    
    func updatePreview(newPreview:GPUImageOutput)
    {
        if let oldPreview:GPUImageOutput = self.currentPreview
        {
            oldPreview.removeTarget(self.previewView);
        }
        
        self.currentPreview = newPreview;
        
        self.currentPreview!.addTarget(self.previewView);
    }
    
    //----------------------------------
    //  MARK: UICollectionViewDataSource
    //----------------------------------
    
    @available(iOS 6.0, *)
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.viewModel.getTotalLooks();
    }
    
    @available(iOS 6.0, *)
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let previewCell: LookPreviewCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("PreviewLookCell", forIndexPath: indexPath) as! LookPreviewCollectionViewCell;
        
        return previewCell;
    }
    
    //----------------------------------
    //  MARK: UICollectionViewDelegate
    //----------------------------------
    
    @available(iOS 8.0, *)
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath)
    {
        let look = self.viewModel.getLookByIndex(indexPath.row);
        
        if let previewCell = cell as? LookPreviewCollectionViewCell
        {
            previewCell.lookPreviewView.fillMode = GPUImageFillModeType(2);
            previewCell.lookNameLabel.text = look.name;

            self.viewModel.setupFilterForPreview(look, view: previewCell.lookPreviewView);
        }
    }
    
    @available(iOS 6.0, *)
    func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath)
    {
        let look = self.viewModel.getLookByIndex(indexPath.row);
        
        if let previewCell = cell as? LookPreviewCollectionViewCell
        {
            self.viewModel.clearFilterFromPreview(look, view: previewCell.lookPreviewView);
        }
    }
    
    @available(iOS 6.0, *)
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        self.viewModel.selectLook(self.viewModel.getLookByIndex(indexPath.row));
//        
//        if let previewCell = collectionView.cellForItemAtIndexPath(indexPath) as? LookPreviewCollectionViewCell
//        {
//            previewCell.markCellAsSelected();
//        }
    }
    
    @available(iOS 6.0, *)
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath)
    {
//        if let previewCell = collectionView.cellForItemAtIndexPath(indexPath) as? LookPreviewCollectionViewCell
//        {
//            previewCell.unmarkCellAsSelected();
//        }
    }
    
    @available(iOS 2.0, *)
    func scrollViewWillBeginDecelerating(scrollView: UIScrollView)
    {
        self.viewModel.pausePreview();
    }
    
    @available(iOS 2.0, *)
    func scrollViewDidEndDecelerating(scrollView: UIScrollView)
    {
        self.viewModel.resumePreview();
    }

    //-------------------------------------
    //  MARK: MovieLooksViewModelDelegate
    //-------------------------------------
    
    func looksViewModelDidLookChange(newLook: Look)
    {
        self.updatePreview(newLook.filterForPreview);
        
        self.applyBarButtonItem.enabled = self.viewModel.selectedLook != nil;
    }
    
    func looksViewModelDidSnapshotChange(newSnapshot:GPUImagePicture)
    {
        if self.currentPreview is GPUImagePicture || self.currentPreview == nil
        {
            self.updatePreview(newSnapshot);
        }

        self.previewPageControl.currentPage = self.viewModel.currentPreviewIndex;
    }
    
    func looksViewModelDidLookProgress(progress:Float)
    {
        // does nothing
    }
    
    func looksViewModelDidLookFinish()
    {
        self.performSegueWithIdentifier("GoSharing", sender: nil);
    }
    
    func looksViewModelDidLookFailed(error:NSError)
    {
        let errorAlertController = UIAlertController(title: "Error".localized, message: error.localizedDescription, preferredStyle: .Alert);
        
        errorAlertController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));
        
        self.presentViewController(errorAlertController, animated: true, completion: nil);
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Actions
    //
    //--------------------------------------------------------------------------
    
    @IBAction func onApplyItem(sender: AnyObject)
    {
        self.viewModel.applySelectedLook();
    }
}
