//
//  ViewController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 1/28/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit
import TwitterKit

class MainViewController: UITabBarController, UITabBarControllerDelegate
{
    //--------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //--------------------------------------------------------------------------
    
    var viewModel:MainViewModel!;

    #if DEBUG
    var memoryUsageLabel: UILabel!;
    var memoryUsageTimer: NSTimer!;

    func updateMemoryUsage()
    {
        if let memoryUsed = getUsedMemory()
        {
            self.memoryUsageLabel.text = "\(memoryUsed / 1024 / 1024) Mb";
        }
        else
        {
            self.memoryUsageLabel.text = "N/A";
        }

    }
    #endif

    //--------------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //--------------------------------------------------------------------------

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // setup SVProgressHUD

        SVProgressHUD.setDefaultStyle(.Dark);
        SVProgressHUD.setDefaultMaskType(.Black);
        SVProgressHUD.setDefaultAnimationType(.Flat);
        
        // setup Twitter
        
        TWTRTweetView.appearance().theme = .Light;
        TWTRTweetView.appearance().backgroundColor = Colors.lightBackgroundColor;
        TWTRTweetView.appearance().primaryTextColor = Colors.lightPrimaryTextColor;
        
        // setup memory usage monitor

        #if DEBUG
        self.memoryUsageTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(MainViewController.updateMemoryUsage), userInfo: nil, repeats: true)
        self.memoryUsageLabel = UILabel(frame: CGRectMake(100, 100, 200, 20));
        self.memoryUsageLabel.text = "TEST";
        self.memoryUsageLabel.backgroundColor = UIColor.whiteColor();
        self.view.addSubview(self.memoryUsageLabel);
        self.view.bringSubviewToFront(self.memoryUsageLabel);
        #endif
        
        // setup UITabBarControllerDelegate
        
        self.delegate = self;
        
        // setup nested ViewControllers

        for controller in self.viewControllers!
        {
            if let timelineController = controller as? TimelineNavigationController
            {
                timelineController.viewModel = self.viewModel.newTimelineViewModel();
            }
            else if let accountController = controller as? AccountNavigationController
            {
                accountController.viewModel = self.viewModel.newAccountViewModel();
            }
        }
        
        // create center button over tabbar
        
        let buttonImage:UIImage = UIImage(named: "New Video Button")!;
        
        let button = UIButton(type: .Custom);
        button.autoresizingMask = [.FlexibleRightMargin, .FlexibleLeftMargin, .FlexibleBottomMargin, .FlexibleTopMargin];
        button.setBackgroundImage(buttonImage, forState: .Normal);
        button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        
        button.addTarget(self, action: #selector(MainViewController.newVideoButtonTapped(_:)), forControlEvents: .TouchUpInside);
        
        let k = button.frame.size.height - self.tabBar.frame.size.height;
        
        if k < 0
        {
            // center button in tabBar
            
            button.center = self.tabBar.center;
        }
        else
        {
            // align button to bottom
            
            var center = self.tabBar.center;
            center.y = center.y - k / 2.0;
            button.center = center;
        }
        
        self.view.addSubview(button);
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated);
        
        print("MainViewController.viewWillAppear()");
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning();
    }

    //--------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //--------------------------------------------------------------------------
    
    //---------------------------------------
    //  UITabBarControllerDelegate
    //---------------------------------------

    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool
    {
        if viewController == self.viewControllers![1]
        {
            // disables showing stub controller 
            return false;
        }
        
        return true;
    }
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Actions
    //
    //--------------------------------------------------------------------------
    
    func newVideoButtonTapped(sender:AnyObject?)
    {
        if let movieViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MovieNavigationController") as? MovieNavigationController
        {
            movieViewController.viewModel = self.viewModel.newMovieViewModel();
            
            self.showViewController(movieViewController, sender: nil);
        }
    }
}
