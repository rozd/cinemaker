//
// Created by Max Rozdobudko on 1/2/16.
// Copyright (c) 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import Crashlytics

var GlobalMainQueue: dispatch_queue_t
{
    return dispatch_get_main_queue()
}

var GlobalUserInteractiveQueue: dispatch_queue_t
{
    return dispatch_get_global_queue(Int(QOS_CLASS_USER_INTERACTIVE.rawValue), 0)
}

var GlobalUserInitiatedQueue: dispatch_queue_t
{
    return dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)
}

var GlobalUtilityQueue: dispatch_queue_t
{
    return dispatch_get_global_queue(Int(QOS_CLASS_UTILITY.rawValue), 0)
}

var GlobalBackgroundQueue: dispatch_queue_t
{
    return dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)
}

func delay(delay:Double, closure:()->()) {
    dispatch_after(
        dispatch_time(
            DISPATCH_TIME_NOW,
            Int64(delay * Double(NSEC_PER_SEC))
        ),
        dispatch_get_main_queue(), closure)
}


func time(desc: String, function: ()->() )
{
    let start : UInt64 = mach_absolute_time()
    function()
    let duration : UInt64 = mach_absolute_time() - start
    
    var info : mach_timebase_info = mach_timebase_info(numer: 0, denom: 0)
    mach_timebase_info(&info)
    
    let total = (duration * UInt64(info.numer) / UInt64(info.denom)) / 1_000_000
    print("\(desc): \(total) µs.")
}

func Log(message: String, args: CVarArgType...)
{
    #if DEBUG
        CLSNSLogv(message, getVaList(args));
    #else
        CLSLogv(message, getVaList(args));
    #endif
}

func getUsedMemory() -> UInt64?
{
    // constant
    let MACH_TASK_BASIC_INFO_COUNT = (sizeof(mach_task_basic_info_data_t) / sizeof(natural_t))

    // prepare parameters
    let name   = mach_task_self_
    let flavor = task_flavor_t(MACH_TASK_BASIC_INFO)
    var size   = mach_msg_type_number_t(MACH_TASK_BASIC_INFO_COUNT)

    // allocate pointer to mach_task_basic_info
    let infoPointer = UnsafeMutablePointer<mach_task_basic_info>.alloc(1)

    // call task_info - note extra UnsafeMutablePointer(...) call
    let kerr = task_info(name, flavor, UnsafeMutablePointer(infoPointer), &size)

    // get mach_task_basic_info struct out of pointer
    let info = infoPointer.move()

    // deallocate pointer
    infoPointer.dealloc(1)

    // check return value for success / failure
    if kerr == KERN_SUCCESS
    {
        return info.resident_size;
    }
    else
    {
        let errorString = String(CString: mach_error_string(kerr), encoding: NSASCIIStringEncoding)
        print(errorString ?? "Error: couldn't parse error string")

        return nil;
    }
}

func debugDiscoverySubviewOfView(view:UIView, indent:String)
{
    print(indent, view);

    for subview in view.subviews
    {
        debugDiscoverySubviewOfView(subview, indent: "    \(indent)");
    }
}