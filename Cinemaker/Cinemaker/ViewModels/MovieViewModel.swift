//
//  MovieViewModel.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/2/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

class MovieViewModel : NSObject
{
    //-----------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------------
    
    init(cinemaker: Cinemaker)
    {
        self.cinemaker = cinemaker;

        self.session = cinemaker.createNewMovieSession();
    }

    deinit
    {
        print("MovieViewModel.deinit");
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //-----------------------------------------------------------------------------
    
    let cinemaker: Cinemaker;

    var session: MovieSession?;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-----------------------------------------------------------------------------
    
    func newRecordViewModel() -> MovieRecordViewModel
    {
        return MovieRecordViewModel(session: self.session!);
    }
    
    func newLooksViewModel() -> MovieLooksViewModel
    {
        return MovieLooksViewModel(session: self.session!);
    }
    
    func newShareViewModel() -> MovieShareViewModel
    {
        return MovieShareViewModel(cinemaker: self.cinemaker, session: self.session!, account: self.cinemaker.account);
    }

    func teardown()
    {
        self.session?.teardown();
        self.session = nil;
    }
}