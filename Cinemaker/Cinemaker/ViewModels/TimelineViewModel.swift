//
//  TimelineViewModel.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/6/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import TwitterKit

@objc protocol TimelineViewModelDelegate : class
{
    func timelineViewModelDidBlockUserSuccess(screenName:String);
    
    func timelineViewModelDidReportTweetSuccess();
    
    func timelineViewModelDidReportIssueSuccess();
    
    func timelineViewModelDidErrorOccurs(error:NSError);

    optional func timelineViewModelDidRequestTerminate();
}

class TimelineViewModel : NSObject
{
    //-----------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------
    
    init(cinemaker:Cinemaker, account:TwitterAccount)
    {
        Log("TimelineViewModel.init");
        
        self.cinemaker = cinemaker;
        self.account = account;
    }
    
    deinit
    {
        Log("TimelineViewModel#deinit");
    }
    
    //-----------------------------------------------------------------------
    //
    //  MARK: - Variables
    //
    //-----------------------------------------------------------------------
    
    let cinemaker:Cinemaker;
    
    let account:TwitterAccount;
    
    //-----------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //-----------------------------------------------------------------------
    
    weak var delegate:TimelineViewModelDelegate?;
    
    var twitterUser: String
    {
        return CinemakerTwitterUserName;
    }
    
    var twitterCollectionId: String
    {
        return CinemakerTwitterCollection;
    }
    
    //-----------------------------------------------------------------------
    //
    //  MARK: - Methods
    //
    //-----------------------------------------------------------------------
    
    func reactivate()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TimelineViewModel.userBlockDidSuccessNotificationHandler(_:)), name: TwitterAccountNotification.UserBlockDidSuccess.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TimelineViewModel.userBlockDidFailedNotificationHandler(_:)), name: TwitterAccountNotification.UserBlockDidFailed.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TimelineViewModel.userBlockDidTerminateNotificationHandler(_:)), name: TwitterAccountNotification.UserBlockDidTerminate.rawValue, object: nil);

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TimelineViewModel.reportDidTerminatedNotificationHandler(_:)), name: CinemakerNotification.ReportDidTerminated.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TimelineViewModel.reportIssueDidFailedNotificationHandler(_:)), name: CinemakerNotification.ReportIssueDidFailed.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TimelineViewModel.reportIssueDidSuccessNotificationHandler(_:)), name: CinemakerNotification.ReportIssueDidSuccess.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TimelineViewModel.reportTweetDidFailedNotificationHandler(_:)), name: CinemakerNotification.ReportTweetDidFailed.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TimelineViewModel.reportTweetDidSuccessNotificationHandler(_:)), name: CinemakerNotification.ReportTweetDidSuccess.rawValue, object: nil);
    }
    
    func deactivate()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self);
    }
    
    func blockUser(screenName:String)
    {
        self.account.blockUser(screenName);
    }
    
    func reportTweet(tweet:TWTRTweet)
    {
        self.cinemaker.reportTweet(tweet);
    }
    
    func reportIssueWithUserBlock(details:String?)
    {
        self.cinemaker.reportIssue("User Block Issue", details: details);
    }
    
    func flagTweet(tweet:TWTRTweet, viewController:UIViewController, sourceView: UIView)
    {
        Log("TimelineViewModel.flagTweet()");

        let flagController:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet);
        
        flagController.addAction(UIAlertAction(title: "Report".localized, style: .Default, handler: { (action:UIAlertAction) -> Void in

            Log("Will report tweet \(tweet)");

            self.reportTweet(tweet);
            
        }));
        
        flagController.addAction(UIAlertAction(title: "Block".localized, style: .Default, handler: { (action:UIAlertAction) -> Void in

            Log("Will block user");

            if let screenName = self.getOriginAuthorScreenNameFromTweet(tweet)
            {
                let blockController = UIAlertController(title: "Block @\(screenName)?", message: "@{name} will be blocked on Twitter but you will continue see their Tweets in Cinemaker's Timeline because Twitter doesn't hides them for you in this case.".localized.substitute(["name" : screenName]), preferredStyle: .Alert);
                
                blockController.addAction(UIAlertAction(title: "Cancel".localized, style: .Cancel, handler: nil));
                
                blockController.addAction(UIAlertAction(title: "Block".localized, style: .Default, handler: { (action:UIAlertAction) -> Void in

                    Log("Will block user @\(screenName)");

                    self.blockUser(screenName);
                    
                }));
                
                viewController.presentViewController(blockController, animated: true, completion: nil);
            }
            else
            {
                Log("Can't resolve user screen name to block");

                let errorController = UIAlertController(title: "Error".localized, message: "Could not block the User. Would you like to send an anonymous report for this issue?", preferredStyle: .Alert);
                
                errorController.addAction(UIAlertAction(title: "Cancel".localized, style: .Cancel, handler: nil));
                
                errorController.addAction(UIAlertAction(title: "Report".localized, style: .Default, handler: { (action:UIAlertAction) -> Void in

                    Log("Will report issue with user blocking");

                    self.reportIssueWithUserBlock(tweet.permalink.absoluteString);
                    
                }));
                
                viewController.presentViewController(errorController, animated: true, completion: nil);
            }
            
        }));
        
        flagController.addAction(UIAlertAction(title: "Cancel".localized, style: .Cancel, handler: nil));
        
        flagController.popoverPresentationController?.sourceView = sourceView;
        flagController.popoverPresentationController?.sourceRect = sourceView.bounds;
        
        viewController.presentViewController(flagController, animated: true, completion: nil);
    }
    
    private func getOriginAuthorScreenNameFromTweet(tweet:TWTRTweet) -> String?
    {
        if let retweeted = tweet.retweetedTweet
        {
            return retweeted.author.screenName;
        }
        else
        {
            if var text = tweet.valueForKey("_text") as? String
            {
                if let range = text.rangeOfString("@\\w+", options: .RegularExpressionSearch)
                {
                    text = text.substringWithRange(range);
                    
                    text = text.substringFromIndex(text.startIndex.advancedBy(1));
                    
                    return text;
                }
            }
        }
        
        return nil;
    }
    
    //------------------------------------
    //  MARK: - Notification handlers
    //------------------------------------

    // MARK: Cinemaker

    func reportIssueDidSuccessNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.timelineViewModelDidReportIssueSuccess();
        }
    }

    func reportIssueDidFailedNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.timelineViewModelDidErrorOccurs(notification.userInfo!["error"] as! NSError);
        }
    }

    func reportTweetDidFailedNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.timelineViewModelDidErrorOccurs(notification.userInfo!["error"] as! NSError);
        }
    }

    func reportTweetDidSuccessNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.timelineViewModelDidReportTweetSuccess();
        }
    }

    func reportDidTerminatedNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.timelineViewModelDidRequestTerminate?();
        }
    }

    // MARK: TwitterAccount
    
    func userBlockDidSuccessNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.timelineViewModelDidBlockUserSuccess(notification.userInfo!["value"] as! String);
        }
    }
    
    func userBlockDidFailedNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.timelineViewModelDidErrorOccurs(notification.userInfo!["error"] as! NSError);
        }
    }
    
    func userBlockDidTerminateNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.timelineViewModelDidRequestTerminate?();
        }
    }
}