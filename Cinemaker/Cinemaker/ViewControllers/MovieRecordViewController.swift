//
//  MovieRecordViewController.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 1/31/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import UIKit
import GPUImage

class RecordingViewController: UIViewController, MovieRecordViewModelDelegate
{
    //-----------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------------
    
    deinit
    {
        print("RecordingViewController.deinit");
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //-----------------------------------------------------------------------------
    
    //--------------------------------------
    //  MARK: Outlets
    //--------------------------------------
    
    @IBOutlet weak var closeBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var nextBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var displayView: GPUImageView!
    @IBOutlet weak var gridView: UIGridView!
    @IBOutlet weak var ghostView: UIImageView!;
    
    @IBOutlet weak var fullscreenButton: UIButton!
    @IBOutlet weak var ghostModeButton: UIButton!
    @IBOutlet weak var showGridButton: UIButton!
//    @IBOutlet weak var horizonButton: UIButton!
    
    @IBOutlet weak var flipCameraButton: UIButton!
    @IBOutlet weak var flashCameraButton: UIButton!

    @IBOutlet weak var previewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var previewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var previewWidthConstraint: NSLayoutConstraint!
   
    //--------------------------------------
    //  MARK: Other views
    //--------------------------------------
    
    var navigationProgressLayer: CAProgressLayer!;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //-----------------------------------------------------------------------------
    
    var viewModel: MovieRecordViewModel!;
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Overridden methods
    //
    //-----------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        self.viewModel.delegate = self;
        
        self.navigationProgressLayer = CAProgressLayer(trackColor: Colors.blueCharade.CGColor, progressColor: Colors.blueMarguerite.CGColor);
        self.navigationController!.navigationBar.layer.sublayers?[0].insertSublayer(self.navigationProgressLayer, atIndex: 1);
        self.navigationProgressLayer.frame = CGRectMake(0.0, 20.0, CGRectGetWidth(self.navigationController!.navigationBar.bounds), CGRectGetHeight(self.navigationController!.navigationBar.bounds));
        self.navigationProgressLayer.setNeedsDisplay();
        
        self.previewWidthConstraint.constant = CGRectGetWidth(self.view.bounds);
        self.previewHeightConstraint.constant = CGRectGetWidth(self.view.bounds);
        self.view.layoutIfNeeded();
        
        self.recordViewModelInputDidRenew(self.viewModel.cameraInput);
        self.recordViewModelInputDidUpdate();
        
        self.flipCameraButton.hitTestEdgeInsets = UIEdgeInsetsMake(-8, -16, -8, -16);
        self.flashCameraButton.hitTestEdgeInsets = UIEdgeInsetsMake(-8, -16, -8, -16);
        
        self.fullscreenButton.hitTestEdgeInsets = UIEdgeInsetsMake(-10, -10, -10, -10);
        self.ghostModeButton.hitTestEdgeInsets = UIEdgeInsetsMake(-10, -10, -10, -10);
        self.showGridButton.hitTestEdgeInsets = UIEdgeInsetsMake(-10, -10, -10, -10);
//        self.horizonButton.hitTestEdgeInsets = UIEdgeInsetsMake(-10, -10, -10, -10);
    }

    override func viewWillAppear(animated: Bool)
    {
        print("RecordingViewController.viewWillAppear");

        super.viewWillAppear(animated);

        self.viewModel.reactivate();
        
        self.navigationProgressLayer.hidden = false;
        
//        print("analyze before");
//        self.analyzeView(self.navigationController!.navigationBar, prefix: "");

//        self.navigationController!.navigationBar.layer.insertSublayer(self.navigationProgressLayer, atIndex: 1);
//        self.navigationProgressLayer.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(self.navigationController!.navigationBar.bounds), CGRectGetHeight(self.navigationController!.navigationBar.bounds));
//        self.navigationProgressLayer.setNeedsDisplay();
        
//        print("analyze after");
//        self.analyzeView(self.navigationController!.navigationBar, prefix: "");
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated);

        self.viewModel.deactivate();
        
        self.navigationProgressLayer.hidden = true;

//        self.navigationProgressLayer.removeFromSuperlayer();
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning();
        
        // Dispose of any resources that can be recreated.
    }
    
    //-----------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-----------------------------------------------------------------------------
    
    func analyzeView(view: UIView, prefix:String)
    {
        print("\(prefix)\(view)");
        
        for subview in view.subviews
        {
            self.analyzeView(subview, prefix: prefix + "    ");
        }
    }
    
    //--------------------------------------
    //  Methods: RecordingViewModelDelegate
    //--------------------------------------
    
    func recordViewModelInputDidRenew(input:AnyObject)
    {
        if let gpuImageCamera = input as? GPUImageVideoCamera
        {
            gpuImageCamera.addTarget(self.displayView);
        }
//        else if let horizonCamera = input as HorizonCamera
//        {
//            // TODO
//        }
    }
    
    func recordViewModelInputDidUpdate()
    {
        // handle fullscreen

        if self.viewModel.cameraFullscreen
        {
            self.displayView.fillMode = GPUImageFillModeType(0);
            
//            self.previewTopConstraint.constant = 0;
            self.previewWidthConstraint.constant = CGRectGetWidth(self.view.bounds);
            self.previewHeightConstraint.constant = CGRectGetHeight(self.view.bounds);
        }
        else
        {
            self.displayView.fillMode = GPUImageFillModeType(2);

//            self.previewTopConstraint.constant = 64;
            self.previewWidthConstraint.constant = CGRectGetWidth(self.view.bounds);
            self.previewHeightConstraint.constant = CGRectGetWidth(self.view.bounds);
        }
        
        self.view.layoutIfNeeded();
        
        // handle orientation
        
        let sy:CGFloat = self.displayView.bounds.size.width / self.displayView.bounds.size.height;
        let sx:CGFloat = self.displayView.bounds.size.height / self.displayView.bounds.size.width;
        
        switch self.viewModel.cameraOrientation
        {
            case .LandscapeRight :
                
                var transform:CGAffineTransform = CGAffineTransformMakeRotation(90.0 * CGFloat(M_PI) / 180.0);
                
                if self.viewModel.cameraFullscreen
                {
                    transform = CGAffineTransformScale(transform, sx, sy);
                }
                
                self.displayView.transform = transform;
                
//                print("after rotation bounds: \(self.displayView.bounds) frame: \(self.displayView.frame)");
            
            case .LandscapeLeft :
                
                var transform:CGAffineTransform = CGAffineTransformMakeRotation(270.0 * CGFloat(M_PI) / 180.0);
                
                if self.viewModel.cameraFullscreen
                {
                    transform = CGAffineTransformScale(transform, sx, sy);
                }
                
                self.displayView.transform = transform;
                
//                print("after rotation bounds: \(self.displayView.bounds) frame: \(self.displayView.frame)");
            
                
            default :
                self.displayView.transform = CGAffineTransformIdentity;
                
//                print("after rotation bounds: \(self.displayView.bounds) frame: \(self.displayView.frame)");
        }
        
        self.gridView.setNeedsDisplay();
    }
    
    func recordViewModelRecordDidStart()
    {
        self.fullscreenButton.enabled = false;
        
//        self.horizonButton.enabled = false;
    }
    
    func recordViewModelRecordProgress(progress:Float, minimumReached:Bool, maximumReached:Bool)
    {
        self.nextBarButtonItem.enabled = minimumReached;
        
        self.navigationProgressLayer.progress = CGFloat(progress);
    }
    
    func recordViewModelRecordDidFinish()
    {
        if self.navigationController?.topViewController == self && UIApplication.sharedApplication().applicationState == .Active
        {
            self.performSegueWithIdentifier("GoPreviewSegue", sender: nil);
        }
    }
    
    func recordViewModelRecordDidFailed(error: NSError)
    {
        let errorAlertController = UIAlertController(title: "Error".localized, message: error.localizedDescription, preferredStyle: .Alert);
        
        errorAlertController.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: { (action: UIAlertAction) -> Void in
            
            self.dismissViewControllerAnimated(true, completion: nil);
        }));
        
        self.presentViewController(errorAlertController, animated: true, completion: nil);
    }

    func recordViewModelRecordDidTerminate()
    {
        self.dismissViewControllerAnimated(false, completion: nil);
    }
    
    func recordViewModelGhostDidChange(active: Bool, snapshot: UIImage?)
    {
        self.ghostView.hidden = !active;
        
        self.ghostView.image = snapshot;
    }

    func recordViewModelShowInfo(message: String)
    {
        let infoAlertDialog = UIAlertController(title: "Info".localized, message: message, preferredStyle: .Alert);

        infoAlertDialog.addAction(UIAlertAction(title: "OK".localized, style: .Default, handler: nil));

        self.presentViewController(infoAlertDialog, animated: true, completion: nil);
    }
    
    //--------------------------------------
    //  Methods: Layout
    //--------------------------------------
    
    //--------------------------------------------------------------------------
    //
    //  MARK: Touches handlers
    //
    //--------------------------------------------------------------------------
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        if (touches.count == 1)
        {
            self.viewModel.resumeRecord();
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        if (touches.count == 1)
        {
            self.viewModel.pauseRecord();
        }
    }
    //-----------------------------------------------------------------------------
    //
    //  MARK: - Actions
    //
    //-----------------------------------------------------------------------------
    
    // MARK: Navigation
    
    @IBAction func onCloseItem(sender: AnyObject)
    {
        self.viewModel.attemptToClose(self, barButtonItem: sender as! UIBarButtonItem) { [unowned self] (interrupt: Bool) -> () in
            
            if !interrupt
            {
                self.navigationController?.dismissViewControllerAnimated(true, completion: nil);
            }
        }
    }
    
    @IBAction func onNextItem(sender: AnyObject)
    {
        self.performSegueWithIdentifier("GoPreviewSegue", sender: nil);
    }
    
    // MARK: Camera buttons
    
    @IBAction func flipCameraButtonTapped(sender: AnyObject)
    {
        self.viewModel.toggleCameraPosition();
    }
    
    @IBAction func flashCameraButtonTapped(sender: AnyObject)
    {
        self.viewModel.toggleCameraFlash();
    }
    
    // MARK: Stack buttons
    
    @IBAction func fullscreenButtonTapped(sender: AnyObject)
    {
        self.fullscreenButton.selected = !self.fullscreenButton.selected;
        
        self.viewModel.toggleCameraFullscreen();
    }
    
    @IBAction func ghostModeButtonTapped(sender: AnyObject)
    {
        self.ghostModeButton.selected = !self.ghostModeButton.selected;
        
        self.viewModel.toggleGhostMode();
    }
    
    @IBAction func showGridButtonTapped(sender: AnyObject)
    {
        self.showGridButton.selected = !self.showGridButton.selected;
        
        self.viewModel.toggleGridMode();
        
        self.gridView.hidden = !self.gridView.hidden;
    }
    
//    @IBAction func lockHorizonButtonTapped(sender: AnyObject)
//    {
//        self.horizonButton.selected = !self.horizonButton.selected;
//        
//        self.viewModel.toggleHorizonMode();
//    }

}
