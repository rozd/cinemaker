//
//  GPURecordService.swift
//  Cinemaker
//
//  Created by Max Rozdobudko on 2/9/16.
//  Copyright © 2016 Max Rozdobudko. All rights reserved.
//

import Foundation

class GPURecordService: RecordService
{
    //-----------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-----------------------------------------------------------------------------

    required init(outputURL: NSURL)
    {
        self.outputURL = outputURL;

        switch UIDevice.currentDevice().orientation
        {
            case .LandscapeLeft :
                self._cameraOrientation = .LandscapeRight;

            case .LandscapeRight :
                self._cameraOrientation = .LandscapeLeft;

            default :
                self._cameraOrientation = .Portrait;
        }

        self.videoCamera = GPUImageVideoCamera(sessionPreset: AVCaptureSessionPresetHigh, cameraPosition: .Back);
        self.videoCamera.outputImageOrientation = self.cameraOrientation;
        self.videoCamera.addAudioInputsAndOutputs();
        self.videoCamera.startCameraCapture();

        self.renewWriter();
    }

    deinit
    {
        print("GPURecordService.deinit");

        self.videoCamera.audioEncodingTarget = nil;
        self.videoCamera.stopCameraCapture();
        self.videoCamera.removeAllTargets();

        self.squareFilter?.removeTarget(self.movieWriter);
        self.squareFilter = nil;

        self.movieWriter?.cancelRecording();
        self.movieWriter = nil;
    }

    //-----------------------------------------------------------------------------
    //
    //  MARK: Variables
    //
    //-----------------------------------------------------------------------------

    private let outputURL: NSURL;

    private var videoCamera: GPUImageVideoCamera;

    private var movieWriter: GPUImageMovieWriter?;

    private var squareFilter: GPUImageCropFilter?;

    private var snapshotFilter: GPUImageFilter?;

    private var backgroundTaskId: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid;

    //-----------------------------------------------------------------------------
    //
    //  MARK: Properties
    //
    //-----------------------------------------------------------------------------

    //--------------------------------------
    //  delegate
    //--------------------------------------

    weak var delegate: RecordServiceDelegate?;

    //--------------------------------------
    //  Properties: Recording
    //--------------------------------------

    var isRecording: Bool = false;

    var isProcessing: Bool = false;

    var isFinished: Bool = false;

    //--------------------------------------
    //  currentInput
    //--------------------------------------

    var currentInput:AnyObject
    {
        return self.videoCamera;
    }

    //--------------------------------------
    //  currentDuration
    //--------------------------------------

    var currentDuration:CMTime
    {
        return self.movieWriter?.duration ?? kCMTimeZero;
    }

    //--------------------------------------
    //  cameraFlashlight
    //--------------------------------------

    var cameraFlashlight:Bool = false
    {
        didSet
        {
            if let camera = self.videoCamera.inputCamera
            {
                do
                {
                    try camera.lockForConfiguration();

                    if self.cameraFlashlight && !camera.torchActive && !camera.flashActive
                    {
                        if camera.hasTorch && camera.isTorchModeSupported(.On)
                        {
                            camera.torchMode = .On;
                        }
                        else if camera.hasFlash && camera.isFlashModeSupported(.On)
                        {
                            camera.flashMode = .On;
                        }
                    }
                    else if camera.torchActive || camera.flashActive
                    {
                        if camera.hasTorch
                        {
                            camera.torchMode = .Off;
                        }
                        else if camera.hasFlash
                        {
                            camera.flashMode = .Off;
                        }
                    }

                    camera.unlockForConfiguration();
                }
                catch let error as NSError
                {
                    print(error);

                    // TODO add logging
                }
            }
        }
    }

    //--------------------------------------
    //  cameraFullscreen
    //--------------------------------------

    private var _cameraFullScreen:Bool = false;

    var cameraFullscreen:Bool
    {
        get
        {
            return self._cameraFullScreen;
        }
        set
        {
            if !self.isProcessing
            {
                self._cameraFullScreen = newValue;

                self.renewWriter();
            }
        }
    }

    //-------------------------------------
    //  cameraPosition
    //-------------------------------------

    private var _cameraOrientation:UIInterfaceOrientation;

    var cameraOrientation:UIInterfaceOrientation
    {
        get
        {
            return self._cameraOrientation;
        }
        set
        {
            if !self.isProcessing
            {
                self._cameraOrientation = newValue;

                self.videoCamera.outputImageOrientation = self._cameraOrientation;

                self.renewWriter();
            }
        }
    }

    //-----------------------------------------------------------------------------
    //
    //  MARK: Methods
    //
    //-----------------------------------------------------------------------------

    func teardown()
    {
        self.videoCamera.stopCameraCapture();
        self.removeWriterIfExists();
    }

    //--------------------------------------
    //  MARK: Camera
    //--------------------------------------

    func flipCamera()
    {
        self.videoCamera.rotateCamera();
    }

    func pauseCamera()
    {
        runSynchronouslyOnVideoProcessingQueue
        {
            self.videoCamera.pauseCameraCapture();
        }
    }

    func resumeCamera()
    {
        runSynchronouslyOnVideoProcessingQueue
        {
            self.videoCamera.resumeCameraCapture();
        }
    }

    //--------------------------------------
    //  Methods: Recording
    //--------------------------------------

    func pauseRecord()
    {
        self.isRecording = false;

        self.movieWriter!.paused = true;
    }

    func resumeRecord()
    {
        self.beginBackgroundTask();

        self.isRecording = true;
        self.isProcessing = true;

        self.movieWriter!.paused = false;
    }

    func finishRecord(completion: ((success: Bool, error: NSError?) -> ())?)
    {
        self.isRecording = false;

        self.movieWriter!.paused = true;

        self.movieWriter!.finishRecordingWithCompletionHandler()
        { [unowned self] in

            self.isProcessing = false;

            self.isFinished = true;

            self.endBackgroundTask();

            if self.movieWriter!.assetWriter.status == .Failed
            {
                dispatch_async(GlobalMainQueue)
                {
                    completion?(success: false, error: self.movieWriter!.assetWriter.error!);

                    self.delegate?.recordServiceDidRecordFailed(self.movieWriter!.assetWriter.error!);
                }
            }
            else
            {
                dispatch_async(GlobalMainQueue)
                {
                    completion?(success: true, error: nil);

                    self.delegate?.recordServiceDidRecordFinish();
                }
            }
        }
    }

    private func terminateRecord()
    {
        self.isProcessing = false;

        self.movieWriter!.cancelRecording();

        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.recordServiceDidRecrodTerminate();
        }
    }

    //--------------------------------------
    //  Methods: Background
    //--------------------------------------

    func beginBackgroundTask()
    {
        if self.backgroundTaskId == UIBackgroundTaskInvalid
        {
            self.backgroundTaskId = UIApplication.sharedApplication().beginBackgroundTaskWithName("GPURecordService.backgroundTask")
            { [unowned self] in

                self.terminateRecord();

                self.endBackgroundTask();
            }
        }

    }

    func endBackgroundTask()
    {
        if self.backgroundTaskId != UIBackgroundTaskInvalid
        {
            UIApplication.sharedApplication().endBackgroundTask(self.backgroundTaskId);

            self.backgroundTaskId = UIBackgroundTaskInvalid;
        }
    }

    //--------------------------------------
    //  Methods: Snapshots
    //--------------------------------------

    func createSnapshot() -> UIImage?
    {
        var image:UIImage?;

        if self.squareFilter != nil
        {
            self.squareFilter!.useNextFrameForImageCapture();
            image = self.squareFilter!.imageFromCurrentFramebuffer();
        }
        else
        {
            self.snapshotFilter = GPUImageFilter();
            self.videoCamera.addTarget(self.snapshotFilter!);

            self.snapshotFilter!.useNextFrameForImageCapture();
            image = self.snapshotFilter!.imageFromCurrentFramebuffer();

            self.videoCamera.removeTarget(self.snapshotFilter);
            self.snapshotFilter = nil;
        }

        return image;
    }

    //-----------------------------------------------------------------------------
    //
    //  MARK: Private methods
    //
    //-----------------------------------------------------------------------------

    private func removeWriterIfExists()
    {
        if self.movieWriter != nil
        {
            if self.squareFilter != nil
            {
                self.squareFilter?.removeTarget(self.movieWriter!)
            }
            else
            {
                self.videoCamera.removeTarget(self.movieWriter!);
            }

            self.videoCamera.audioEncodingTarget = nil;
            self.movieWriter!.cancelRecording();
            self.movieWriter = nil;
        }
    }

    private func renewWriter()
    {
        self.removeWriterIfExists();

        let sizeAndCrop = self.getOutputSizeAndCropForCurrentSettings();
        
        let size = sizeAndCrop.size;
        let crop = sizeAndCrop.crop;

        unlink(self.outputURL.fileSystemRepresentation);

        self.movieWriter = GPUImageMovieWriter(movieURL: self.outputURL, size: size, fileType: AVFileTypeQuickTimeMovie, outputSettings: self.bestQualityJPEG());
        self.movieWriter!.assetWriter.movieFragmentInterval = kCMTimeInvalid;

        self.movieWriter?.failureBlock = { [unowned self] (error:NSError!) in
            
            self.delegate?.recordServiceDidRecordFailed(error);
        }

        if crop != nil
        {
            if self.squareFilter == nil
            {
                self.squareFilter = GPUImageCropFilter(cropRegion: crop!);
                self.videoCamera.addTarget(self.squareFilter!);
            }
            else
            {
                self.squareFilter!.cropRegion = crop!;
            }

            self.squareFilter!.addTarget(self.movieWriter);
        }
        else
        {
            if self.squareFilter != nil
            {
                self.squareFilter!.removeTarget(self.movieWriter);
                self.videoCamera.removeTarget(self.squareFilter!);
                self.squareFilter = nil;
            }

            self.videoCamera.addTarget(self.movieWriter!);
        }

        self.videoCamera.audioEncodingTarget = self.movieWriter!;
        self.movieWriter!.startRecording();
        self.movieWriter!.paused = true;
    }
    
    //  MARK: Output size
    
    func getOutputSizeAndCropForCurrentSettings() -> (size: CGSize, crop: CGRect?)
    {
        let p1920: CGFloat = 1280.0;
        let p1080: CGFloat = 720.0;
        
        var size:CGSize!;
        var crop:CGRect?;
        
        let ratio:Float = Float((p1920 - p1080) / p1920);
        
        switch self.cameraOrientation
        {
            case .LandscapeLeft : fallthrough;
            case .LandscapeRight :
                
                if self.cameraFullscreen
                {
                    size = CGSizeMake(p1920, p1080);
                }
                else
                {
                    size = CGSizeMake(p1080, p1080);
                    crop = CGRectMake(CGFloat(ratio / 2.0), 0, 1 - CGFloat(ratio), 1);
                }
                
            default :
                
                if self.cameraFullscreen
                {
                    size = CGSizeMake(p1080, p1920);
                }
                else
                {
                    size = CGSizeMake(p1080, p1080);
                    crop = CGRectMake(0, CGFloat(ratio / 2.0), 1, 1 - CGFloat(ratio));
                }
        }
        
        return (size, crop);
    }
    
    //  MARK: Output settings
    
    func animationQuality() -> [String : AnyObject]
    {
        let size = self.getOutputSizeAndCropForCurrentSettings().size;
        
        let settings: [String : AnyObject] =
        [
            AVVideoCodecKey : "rle ",
            AVVideoWidthKey : size.width,
            AVVideoHeightKey : size.height,
        ];
        
        return settings;
    }
    
    func bestQualityH264() -> [String : AnyObject]
    {
        let size = self.getOutputSizeAndCropForCurrentSettings().size;
        
        let settings:[String:AnyObject] =
        [
            AVVideoCodecKey : AVVideoCodecH264,
            AVVideoWidthKey : size.width,
            AVVideoHeightKey : size.height,
//            AVVideoAverageNonDroppableFrameRateKey : 24,
            
            AVVideoPixelAspectRatioKey :
            [
                AVVideoPixelAspectRatioHorizontalSpacingKey : 1,
                AVVideoPixelAspectRatioVerticalSpacingKey : 1
            ],
            
            AVVideoCompressionPropertiesKey :
            [
                AVVideoProfileLevelKey : AVVideoProfileLevelH264High41,
                AVVideoMaxKeyFrameIntervalKey : 1,
                AVVideoAverageBitRateKey : 10000000, // 10Mbps
                
//                AVVideoMaxKeyFrameIntervalDurationKey : 0.0,
//                AVVideoH264EntropyModeKey : AVVideoH264EntropyModeCABAC,
            ]
        ]
        
        return settings;
    }

    func bestQualityJPEG() -> [String : AnyObject]
    {
        let size = self.getOutputSizeAndCropForCurrentSettings().size;
        
        let settings:[String:AnyObject] =
        [
            AVVideoCodecKey : AVVideoCodecJPEG,
            AVVideoWidthKey : size.width,
            AVVideoHeightKey : size.height,
            AVVideoCompressionPropertiesKey :
            [
                AVVideoQualityKey : 0.9,
            ]
        ]
        
        return settings;
    }
}