//
// Created by Max Rozdobudko on 3/10/16.
// Copyright (c) 2016 Max Rozdobudko. All rights reserved.
//

import Foundation
import TwitterKit

protocol TweetViewModelDelegate: class
{
    func tweetViewModelDidLoginSuccess();
    func tweetViewModelDidLoginCancel();
    func tweetViewModelDidUserChange();
    func tweetViewModelDidErrorOccurs(error:NSError);

    func tweetViewModelTweetDidStatusChange(message:String);
    func tweetViewModelTweetDidSucceess(tweet:TWTRTweet);
}

class TweetViewModel : NSObject
{
    //-------------------------------------------------------------------------
    //
    //  MARK: Lifecycle
    //
    //-------------------------------------------------------------------------

    init(account:TwitterAccount)
    {
        self.account = account;
    }

    deinit
    {
        print("TweetViewModel.deinit");
    }

    //-------------------------------------------------------------------------
    //
    //  MARK: - Variables
    //
    //-------------------------------------------------------------------------

    let account:TwitterAccount;

    //-------------------------------------------------------------------------
    //
    //  MARK: - Properties
    //
    //-------------------------------------------------------------------------

    //  MARK: Delegate

    weak var delegate:TweetViewModelDelegate?;

    //  MARK: Twitter

    var currentUser:TWTRUser?
    {
        return self.account.currentUser;
    }

    var currentSession:TWTRSession?
    {
        return self.account.currentSession;
    }

    //-------------------------------------------------------------------------
    //
    //  MARK: - Methods
    //
    //-------------------------------------------------------------------------

    func reactivate()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TweetViewModel.twitterUserChangeNotificationHandler(_:)), name: TwitterAccountNotification.TwitterUserChange.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TweetViewModel.twitterLoginSuccessNotificationHandler(_:)), name: TwitterAccountNotification.TwitterLoginSuccess.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TweetViewModel.twitterErrorOccursNotificationHandler(_:)), name: TwitterAccountNotification.TwitterErrorOccurs.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TweetViewModel.twitterLoginCancelNotificationHandler(_:)), name: TwitterAccountNotification.TwitterLoginCancel.rawValue, object: nil);

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TweetViewModel.tweetDidStatusNotificationHandler(_:)), name: TwitterAccountNotification.TweetDidStatus.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TweetViewModel.tweetDidSuccessNotificationHandler(_:)), name: TwitterAccountNotification.TweetDidSuccess.rawValue, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TweetViewModel.tweetDidFailedNotificationHandler(_:)), name: TwitterAccountNotification.TweetDidFailed.rawValue, object: nil);

        self.account.loginIfNeeded();
    }

    func deactivate()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self);
    }

    func tweet(caption:String, movieURL: NSURL)
    {
        self.account.updateStatus(caption, mediaURL: movieURL);
    }

    //-------------------------------------------------------------------------
    //
    //  MARK: Notification handlers
    //
    //-------------------------------------------------------------------------

    func twitterLoginSuccessNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.tweetViewModelDidLoginSuccess();
        }
    }

    func twitterLoginCancelNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.tweetViewModelDidLoginCancel();
        }
    }

    func twitterUserChangeNotificationHandler(notification: NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.tweetViewModelDidUserChange();
        }
    }

    func twitterErrorOccursNotificationHandler(notification: NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.tweetViewModelDidErrorOccurs(notification.userInfo!["error"] as! NSError);
        }
    }

    func tweetDidStatusNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            if let statusMessage = notification.userInfo?["value"] as? String
            {
                self.delegate?.tweetViewModelTweetDidStatusChange(statusMessage);
            }
        }
    }

    func tweetDidSuccessNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.tweetViewModelTweetDidSucceess(notification.userInfo!["value"] as! TWTRTweet);
        }
    }

    func tweetDidFailedNotificationHandler(notification:NSNotification)
    {
        dispatch_async(GlobalMainQueue)
        {
            self.delegate?.tweetViewModelDidErrorOccurs(notification.userInfo!["error"] as! NSError);
        }
    }

}
